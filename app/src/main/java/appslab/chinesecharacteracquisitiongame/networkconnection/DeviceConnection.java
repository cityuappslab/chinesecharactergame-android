package appslab.chinesecharacteracquisitiongame.networkconnection;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

/**
 * Created by Johnny on 6/15/16.
 */
public class DeviceConnection {
    DeviceConnection dc;

    static Context ctx;
    private static Handler mHandler = new Handler();
    boolean returnValue = true;

    public DeviceConnection() {

    }

    public boolean checkNetworkAvailability(Context ctx) {
        this.ctx = ctx;

        if(APIConnection.getInstance().isOnline(DeviceConnection.ctx) == false) {
            Toast.makeText(DeviceConnection.ctx, "No Internet! Please access to Internet and try again!", Toast.LENGTH_LONG).show();
            returnValue = false;
            return returnValue;
        }

        mHandler.postDelayed(new Runnable() {
            public void run() {
                if(APIConnection.getInstance().isOnline(DeviceConnection.ctx) == false) {
                    Toast.makeText(DeviceConnection.ctx, "No Internet! Please access to Internet and try again!", Toast.LENGTH_LONG).show();
                    ((Activity)DeviceConnection.ctx).finish();
                    returnValue = false;
                }
            }
        }, 60000);

        return returnValue;
    }

}
