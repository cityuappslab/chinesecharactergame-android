package appslab.chinesecharacteracquisitiongame.networkconnection;

/**
 * Created by appslab on 6/4/16.
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.loopj.android.http.*;

import cz.msebera.android.httpclient.HttpEntity;

public class APIConnection {
    public static APIConnection sharedInstance = null;

//    private static final String BASE_URL = "http://202.125.255.2:8080/";
    private static final String BASE_URL = "http://chinesecharactergame.cloudapp.net/";

    public static APIConnection getInstance() {
        if(sharedInstance == null) {
            sharedInstance = new APIConnection();
        }
        return sharedInstance;
    }

    public boolean isOnline(Context ctx) {
        ConnectivityManager cm =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();

    }

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void login(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl("login"), params, responseHandler);
    }

    public static void register(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl("register"), params, responseHandler);
    }

    public static void logout(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl("logout"), params, responseHandler);
    }

    public static void getStageCompletedDiaglog(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl("stageCompletedDialog"), params, responseHandler);
    }

    public static void postStageCompletedDiaglog(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl("poststage"), params, responseHandler);
    }

    public static void postArray(Context context, HttpEntity entity, AsyncHttpResponseHandler responseHandler) {
        client.post(context, getAbsoluteUrl("records"), entity, "application/json", responseHandler);
    }

    public static void getGameData(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl("game"), params, responseHandler);
    }

    public static void getStageData(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl("stage"), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
