package appslab.chinesecharacteracquisitiongame.networkconnection;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import appslab.chinesecharacteracquisitiongame.gamesetting.Character;

/**
 * Created by SunnyChan on 16/8/2016.
 */
public class MatrixPosition {
    private static final String TAG = MatrixPosition.class.getSimpleName();
    Map<String, String> matrixPosition = new HashMap<>();
    int xPosition;
    int yPosition;
    String character;

    public MatrixPosition() {
        initializeMap();

    }

    public void setMatrixPosition(int xPosition, int yPosition, Character character) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.character = character.getOneCharacter();
        this.matrixPosition.put("row" + (xPosition + 1) + "col" + (yPosition + 1), character.getOneCharacter());
    }


    public void clearHashMap() {
        this.matrixPosition.clear();
        logingHelper("clearHashMap", "Map cleared");
        initializeMap();
    }

    public JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject(matrixPosition);
        logingHelper("getJsonObject", String.valueOf(jsonObject));
        return jsonObject;
    }


    public Map<String, String> getMatrixPositionMap() {
        return this.matrixPosition;
    }

    private void initializeMap(){
        //initialize the map
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 6; j++) {
                this.matrixPosition.put("row" + (i) + "col" + (j), "");
            }
        }
        logingHelper("initializeMap", "Map Initialized");
    }

    public JSONObject getMergedJsonObject(JSONObject obejectToMerge, JSONObject matrixPositionJsonObject) throws JSONException {
        //used to merged to JsonObjects
        JSONObject merged = new JSONObject();
        JSONObject[] objs = new JSONObject[] { obejectToMerge, matrixPositionJsonObject };
        for (JSONObject obj : objs) {
            Iterator it = obj.keys();
            while (it.hasNext()) {
                String key = (String)it.next();
                merged.put(key, obj.get(key));
            }
        }
        logingHelper("getMergedJsonObject", String.valueOf(merged));
        return merged;
    }

    private  void logingHelper(String tag, String message) {
        Log.d(TAG, tag + ": " + message);
    }

}
