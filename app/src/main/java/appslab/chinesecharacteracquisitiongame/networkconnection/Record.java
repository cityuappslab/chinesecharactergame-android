package appslab.chinesecharacteracquisitiongame.networkconnection;

/**
 * Created by appslab on 6/4/16.
 */
public class Record {
    private int timeUsed;
    private int correct;
    private int incorrect;
    private int total;

    public Record() {
    }

    public int getTimeUsed() {
        return timeUsed;
    }

    public void setTimeUsed(int timeUsed) {
        this.timeUsed = timeUsed;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public int getIncorrect() {
        return incorrect;
    }

    public void setIncorrect(int incorrect) {
        this.incorrect = incorrect;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
