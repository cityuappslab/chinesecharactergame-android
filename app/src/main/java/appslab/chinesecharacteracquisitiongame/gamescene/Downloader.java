package appslab.chinesecharacteracquisitiongame.gamescene;

import java.io.IOException;
import java.net.URL;

/**
 * Created by appslab on 7/7/16.
 */
public class Downloader {


    public static long downloadFile(URL url) {
        try {
            return url.openConnection().getContentLength();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
