package appslab.chinesecharacteracquisitiongame.gamescene;

import android.app.Activity;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;
import appslab.chinesecharacteracquisitiongame.R;
import cz.msebera.android.httpclient.Header;

public class Login extends Activity implements View.OnTouchListener {

    ImageButton login;
    private EditText email, password;
    AsyncHttpClient client = new AsyncHttpClient();
    SharedPreferences settings;
    SoundController sc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_main);
        SharedPrefsData.clearSharedPrefernce(getApplicationContext());

        //For playing sound
        sc = new SoundController();

        email = (EditText) findViewById(R.id.editUser);
        password = (EditText) findViewById(R.id.editPwd);

        String storedEmail = SharedPrefsData.getLoginEmail(getApplicationContext());
        if (!storedEmail.equals("")){
            Log.d("sunny", "Login, "+ " storedEmail: "+storedEmail);
            email.setText(storedEmail);
        }

        login = (ImageButton) findViewById(R.id.btnSubmit);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sc.playButtonSoundTrack(Login.this);
                String userName = Login.this.email.getText().toString();
                String password = Login.this.password.getText().toString();

                login(userName, password);
            }
        });
        login.setOnTouchListener(this);
    }

    public void login(String email, String password) {
        String tempEmail;
        String tempPassword;
        if (GeneralUtil.DEBUG_AUTO_LOGIN){
            tempEmail =GeneralUtil.DEBUG_LOGIN_MAIL;
            tempPassword=GeneralUtil.DEBUG_LOGIN_PW;
        }else{
            tempEmail = email;
            tempPassword = password;
        }

        SharedPrefsData.setLoginEmail(getApplicationContext(),tempEmail);
        String storedEmail = SharedPrefsData.getLoginEmail(getApplicationContext());
        Log.d("sunny","SharedPrefsData.getRegisterInfoEmail(getApplicationContext()): "+ storedEmail );

        RequestParams params = new RequestParams();
        params.put("email", tempEmail);
        params.put("password", tempPassword);
        params.put("remember",true);
        params.put("api", "");

        APIConnection.sharedInstance.login(params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                JSONObject obj = response;

                try {
                    String error = String.valueOf(obj.getString("error"));
                    if (error == "true") {  //Login unsuccessfully
                        Toast.makeText(getApplicationContext(), "Login fail.", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), response.getString("msg"), Toast.LENGTH_LONG).show();
//                        Toast.makeText(getApplicationContext(), response.getString("email"), Toast.LENGTH_LONG).show();
                    } else if (error == "false") {  //Login successfully
//                        String email = response.getString("email");
                        JSONObject userInfo = new JSONObject(response.getString("user"));
                        String userName = userInfo.getString("name");

                        Log.w("Login: ", userInfo.getString("name"));
                        showGameActivity(userName);
                        Login.this.password.setText("");
//                        Toast.makeText(getApplicationContext(), "Login: False", Toast.LENGTH_LONG).show();
//                        Toast.makeText(getApplicationContext(), response.getString("msg"), Toast.LENGTH_LONG).show();
//                        Toast.makeText(getApplicationContext(), response.getString("email"), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Login: UNKNOWN", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Login in failed", Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.w("MainActivity-onFailure", "onFailure without JSONARRAY");
                Log.w("MainActivity-onFailure", String.valueOf(statusCode));
                Log.w("MainActivity-onFailure", responseString);
            }
        });
    }

    private void showGameActivity(String username) {
        settings = getSharedPreferences("PREF", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString("USER_NAME", username);
        PE.commit();

        Intent i = new Intent(getApplicationContext(), Games.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

        finish();
    }

    public void getGameData() throws JSONException {
        APIConnection.sharedInstance.getGameData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONArray jsonArrayGames = response.getJSONArray("games");


                } catch (JSONException e) {

                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                // Pull out the first event on the public timeline

            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.5f);
        } else {
            v.setAlpha(1.0f);
        }
        return false;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindDrawables(findViewById(R.id.login_main_content));
    }

    public void unbindDrawables(View view) {//pass your parent view here
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(Login.this,MainActivity.class));
        finish();
    }

    public void onTrimMemory(int level) {

        // Determine which lifecycle or system event was raised.
        switch (level) {

            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                break;

            default:
                break;
        }
    }

}