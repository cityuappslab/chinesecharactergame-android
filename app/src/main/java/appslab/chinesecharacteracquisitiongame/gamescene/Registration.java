package appslab.chinesecharacteracquisitiongame.gamescene;

/**
 * Created by Johnny on 8/6/2016.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;
import cz.msebera.android.httpclient.Header;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;

public class Registration extends Activity {
    // UI references.
    private AutoCompleteTextView email, name;
    private EditText password, passwordConfirmation, regCode;
    private Button registrationButton;
    private ImageButton btnBack;
    private TextView returnMessage;
    private LinearLayout returnMessageLayout, email_login_form;
    //these three String are for the convenience after reg failed
    private String nameString, emailString, registrationCodeString;
    Activity activity;
    //if Successful registration, goto MainActivity onBackPressed
    private boolean isSuccessfulRegister = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.registration);
        // Set up the login form.

        name = (AutoCompleteTextView) findViewById(R.id.name);
        email = (AutoCompleteTextView) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        passwordConfirmation = (EditText) findViewById(R.id.password_confirmation);
        regCode = (EditText) findViewById(R.id.registrarion_code);
        registrationButton = (Button) findViewById(R.id.btnRegistration);
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        returnMessageLayout = (LinearLayout) findViewById(R.id.return_message_layout);
        email_login_form = (LinearLayout) findViewById(R.id.email_login_form);
        activity = Registration.this;

        //to check if the last registration is successful or not, if it's not successful, set back the text input in the nameTV
        //emailTV, regCodeTV
        if (!SharedPrefsData.getRegisterInfoIsSuccessful(getApplicationContext())){
            getRegInfo();
            name.setText(nameString);
            email.setText(emailString);
            regCode.setText(registrationCodeString);
        }

        registrationButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = Registration.this.name.getText().toString();
                String email = Registration.this.email.getText().toString();
                String password = Registration.this.password.getText().toString();
                String passwordConfirmation = Registration.this.passwordConfirmation.getText().toString();
                String regCode = Registration.this.regCode.getText().toString();

                SharedPrefsData.setRegisterInfo(getApplicationContext(), name,email,regCode);

//                Log.d("sunny", "Registration.java, step1, "+ " name: "+ name+ " \temail: "+ email+ " \tregCode: "+ regCode);

                register(name, email, regCode, password, passwordConfirmation);
            }
        });
    }

    private void getRegInfo(){
        nameString = SharedPrefsData.getRegisterInfoName(getApplicationContext());
        emailString = SharedPrefsData.getRegisterInfoEmail(getApplicationContext());
        registrationCodeString = SharedPrefsData.getRegisterInfoRegcode(getApplicationContext());
        Log.d("sunny", "Registration.java, "+ " name: "+ nameString+ " \temail: "+ emailString+ " \tregCode: "+ registrationCodeString);
    }

    public void register(String name, final String email, String regCode, String password, String passwordConfirmation) {
        RequestParams params = new RequestParams();
        params.put("name", name);
        params.put("email", email);
        params.put("reg_code", regCode);
        params.put("password", password);
        params.put("password_confirmation", passwordConfirmation);
        params.put("api", "null");

        APIConnection.sharedInstance.register(params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                JSONObject obj = response;
                JSONObject objMSG = null;

                try {
                    objMSG = new JSONObject(response.getString("msg"));
                    Log.d("sunny", response.getString("msg"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    String error = String.valueOf(obj.getString("error"));

                    if (error == "true") {  //Register unsuccessfully
                        //input false means reg fails
                        resetForm(false);
                        isSuccessfulRegister = false;
                        SharedPrefsData.setRegisterInfoIsSuccessful(getApplicationContext(), false);
                        appendReturnMSG("Registration failed! This might be caused by: ");

                        if (objMSG.has("email")) {
                            appendReturnMSG(objMSG.getString("email").replace("[\"", "").replace("\"]", ""));
                        }

                        if (objMSG.has("name")) {
                            Log.w("JOHNNYDEBUG", response.getString("msg"));
                            appendReturnMSG(objMSG.getString("name").replace("[\"", "").replace("\"]", ""));
                        }

                        if (objMSG.has("password")) {
                            appendReturnMSG(objMSG.getString("password").replace("[\"", "").replace("\"]", ""));
                        }
                        if (objMSG.has("reg_code")) {
                            appendReturnMSG(objMSG.getString("reg_code").replace("[\"", "").replace("\"]", ""));
                        }

                    } else if (error == "false") {  //Register successfully
//                        appendReturnMSG(obj.getString("msg"));
                        /** changed to hard code for "Registration Successful" ,
                         * because the server will return "Register Success and Login successful",
                         * that "and Login successful" is not wanted*/
                        appendReturnMSG("Registration Successful. You can login now!");
                        Log.d("sunny", obj.getString("msg"));
                        SharedPrefsData.setLoginEmail(getApplicationContext(),email);
                        SharedPrefsData.setRegisterInfoIsSuccessful(getApplicationContext(), true);
                        //input true means reg succeeds
                        resetForm(true);
                        isSuccessfulRegister = true;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.w("Registration-onFailure", String.valueOf(statusCode));
                Log.w("Registration-onFailure", responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                JSONArray jsonarray = errorResponse;
                String error;
                try {
//                    Toast.makeText(getApplicationContext(), "onFailure +  JSONARRAY", Toast.LENGTH_LONG).show();
                    error = jsonarray.getString(0);
                    Log.w("Registration-onFailure", "error: " + error);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void resetForm(final boolean isRegSuccessful) {
        //Disappear the components in the form
        disappearView(name);
        disappearView(email);
        disappearView(password);
        disappearView(passwordConfirmation);
        disappearView(registrationButton);
        disappearView(regCode);

        //Appear the hidden button "Back"
//        enlargeButton(btnBack);
        appearView(btnBack);
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

//                backToRegistration();
                Intent mStartActivity;
                if (isRegSuccessful){
                    mStartActivity = new Intent(getApplicationContext(), MainActivity.class);
                    finish();
                }else{
                    mStartActivity = new Intent(getApplicationContext(), Registration.class);
                    finish();
                }
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                System.exit(0);

//                forceReenterTheApp();
//                finish();
            }
        });
    }

    private void forceReenterTheApp() {
        if (activity.isDestroyed()) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
        }
    }

    private void backToRegistration(){
        disappearView(btnBack);
        disappearView(returnMessage);
        appearView(name);
        appearView(email);
        appearView(password);
        appearView(passwordConfirmation);
        appearView(registrationButton);
        appearView(regCode);

    }


    private void disappearView(View v) {
        final View view = v;

        //To fade out the button with 500ms
        view.animate()
                .alpha(0.0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                    }
                });
    }

    private void enlargeButton(Button btn) {
        btn.getLayoutParams().height=200;
    }

    private void appearView(View v) {
        final View view = v;

        //To fade in the button with 500ms
        view.animate()
                .alpha(0.0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void appendReturnMSG(String msg) {
        returnMessage = (TextView) findViewById(R.id.return_message);

        //If the TextView "returnMessage" had been created, then append the return message into it
        if (email_login_form != null) {
            returnMessage.setText(returnMessage.getText() + msg + "\n");
            //returnMessage.append(msg + " \n");

        } else {
            returnMessage = new TextView(getApplicationContext());
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams
                    ((int) RelativeLayout.LayoutParams.WRAP_CONTENT,(int) RelativeLayout.LayoutParams.WRAP_CONTENT);

            returnMessage.setId(R.id.return_message);
            returnMessage.setText(msg + "\n");
            returnMessage.setTextSize((float) 20);
            returnMessage.setPadding(20, 50, 20, 50);
            returnMessage.setLayoutParams(params);
            returnMessage.setTextColor(Color.BLACK);

            email_login_form.addView(returnMessage);
        }
    }

    @Override
    public void onBackPressed() {
            startActivity(new Intent(Registration.this, MainActivity.class));
            finish();

    }

    public void onTrimMemory(int level) {

        // Determine which lifecycle or system event was raised.
        switch (level) {

            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                break;

            default:
                break;
        }
    }
}