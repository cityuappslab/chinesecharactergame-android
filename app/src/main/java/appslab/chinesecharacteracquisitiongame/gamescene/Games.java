package appslab.chinesecharacteracquisitiongame.gamescene;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;
import appslab.chinesecharacteracquisitiongame.R;
import cz.msebera.android.httpclient.Header;

public class Games extends Activity implements OnClickListener, OnTouchListener {

    private static final String TAG = Games.class.getSimpleName();

    ImageView levelUpIcon;
    ImageButton g1, g2, g3, g4, optionLogout;
    TextView editUserDisplayName;
    String email, password, passwordConfirmation, name;
    AsyncHttpClient client = new AsyncHttpClient();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;
    boolean shouldLoadOnResume = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.games_menu);
        shouldLoadOnResume = false;

        initializeUI();
        GeneralUtil.getAllSharedPrefsKeys(getApplicationContext());


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void initializeUI() {
        findResources();
        setUserName();
        setGameLevelBtn();

        //TODO set the stage return here to change the lvUpIcon
//        setLVUPIcon();

        try {
            //set up the icon at the same time
            getStageData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void findResources() {

        editUserDisplayName = (TextView) findViewById(R.id.editUserDisplayName);
        optionLogout = (ImageButton) findViewById(R.id.optionLogout);
        optionLogout.setOnClickListener(this);
        optionLogout.setOnTouchListener(this);
    }

    private void setGameLevelBtn() {

        g1 = (ImageButton) findViewById(R.id.g1);
        g1.setOnClickListener(this);
        g1.setOnTouchListener(this);

        g2 = (ImageButton) findViewById(R.id.g2);
        g2.setOnClickListener(this);
        g2.setOnTouchListener(this);

        g3 = (ImageButton) findViewById(R.id.g3);
        g3.setOnClickListener(this);
        g3.setOnTouchListener(this);

        g4 = (ImageButton) findViewById(R.id.g4);
        g4.setOnClickListener(this);
        g4.setOnTouchListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (shouldLoadOnResume) {
            try {
                getStageData();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        shouldLoadOnResume = true;
    }

    private void setUserName() {
        SharedPreferences settings = getSharedPreferences("PREF", 0);
        String username = settings.getString("USER_NAME", "none");
        editUserDisplayName.setText(username);
    }

    //TODO set LVUP icon
    private void setLVUPIcon() {
        levelUpIcon = (ImageView) findViewById(R.id.levelUpIcon_game_menu);
        //TODO setting the stage number
//        SharedPrefsData.setStageNumber(getApplicationContext(), 3);
        int stage_number = SharedPrefsData.getStageNumber(getApplicationContext());
        switch (stage_number) {
            case 0://egg
                levelUpIcon.setImageResource(R.drawable.lvup_animal1);
                break;
            case 1://cracked egg
                levelUpIcon.setImageResource(R.drawable.lvup_animal2);
                break;
            case 2://little chick
                levelUpIcon.setImageResource(R.drawable.lvup_animal3);
                break;
            case 3://mother chick
                levelUpIcon.setImageResource(R.drawable.lvup_animal4);
                break;

        }
    }


    @Override
    public void onClick(View v) {
        goToNextPage(v);
    }

    public void getStageData() throws JSONException {
        APIConnection.sharedInstance.getStageData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    String error = String.valueOf(response.getString("error"));
                    if (error == "true") {  //Get Game Data unsuccessfully
                        Toast.makeText(getApplicationContext(), "Fail to get the Game Data. ", Toast.LENGTH_LONG).show();
                        finish();
                    } else if (error == "false") {  //Get Game Data successfully
                        int stageNumber_json = Integer.parseInt(response.getString("stage"));
                        if (GeneralUtil.DEBUG_TEST_STAGE) {
                            int stage_number = SharedPrefsData.getStageNumber(getApplicationContext());
                            SharedPrefsData.setStageNumber_debug(getApplicationContext(), stage_number);
                        } else {
                            SharedPrefsData.setStageNumber(getApplicationContext(), stageNumber_json);
                        }
                        /**
                         * only this activity will set the lvUp icon right after refreshing the stage number, other activities
                         * will not
                         */
                        setLVUPIcon();
                        Log.d(this.getClass().getSimpleName() + "Stage Data: ", response.toString());
                        Log.d(this.getClass().getSimpleName() + "Stage Number: ", String.valueOf(stageNumber_json));

                        JSONArray showstage = response.getJSONArray("showstage");
                        for (int i = 0; i < showstage.length(); i++) {
                            JSONObject stage = showstage.getJSONObject(i);
                            int stage_num_temp = Integer.parseInt(stage.getString("stage"));
                            int shown = Integer.parseInt(stage.getString("shown"));

                            boolean needShow = (shown ==1)? false:true;


                            Log.d("sunny", "games.java "+"stage_num_temp: "+stage_num_temp);
                            Log.d("sunny", "games.java "+"needShow: "+needShow);
                            SharedPrefsData.setShouldShowCompletedDialog_Server(getApplicationContext(),needShow, stage_num_temp);
                            boolean temp = SharedPrefsData.getShouldShowStageCompletedDialog_Server(getApplicationContext());
                            Log.d("sunny", i+" getShouldShowStageCompletedDialog_Server: "+ temp);


                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.d(this.getClass().getSimpleName() + "-onFailure", String.valueOf(statusCode));
                Log.d(this.getClass().getSimpleName() + "-onFailure", responseString);
            }
        });
    }


    public void getGameData() throws JSONException {
        APIConnection.sharedInstance.getGameData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONArray jsonArrayGames = response.getJSONArray("games");


                } catch (JSONException e) {

                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                // Pull out the first event on the public timeline

            }
        });
    }

    @Override
    public void onBackPressed() {
        optionLogout.performClick();
    }

    public void goToNextPage(View v) {
        Intent i = null;
        switch (v.getId()) {
            case R.id.g1:
                i = new Intent(getApplicationContext(), Game1LevelMenu.class);
                startActivity(i);
                finish();
                break;
            case R.id.g2:
                i = new Intent(getApplicationContext(), Game4LevelMenu.class);
                startActivity(i);
                finish();
                break;
            case R.id.g3:
                i = new Intent(getApplicationContext(), Game5_Bird_LevelMenu.class);
                startActivity(i);
                finish();
                break;
            case R.id.g4:
                i = new Intent(getApplicationContext(), Game5_Bat_LevelMenu.class);
                startActivity(i);
                finish();
                break;
            case R.id.optionLogout:
                final Dialog dialog = new Dialog(Games.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog);
                dialog.setCancelable(true);
                //there are a lot of settings, for dialog, check them all out!

                //set up image view
                ImageButton ibLogoutYes = (ImageButton) dialog.findViewById(R.id.ibLogoutYes);
                ibLogoutYes.setOnTouchListener(this);
                ibLogoutYes.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        logout();
                        startActivity(new Intent(Games.this,MainActivity.class));
                        finish();
                    }
                });

                ImageButton ibLogoutNo = (ImageButton) dialog.findViewById(R.id.ibLogoutNo);
                ibLogoutNo.setOnTouchListener(this);
                ibLogoutNo.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                //now that the dialog is set up, it's time to show it
                dialog.cancel();
                dialog.show();
                break;
        }
    }

    public void logout() {
        RequestParams params = new RequestParams();

        APIConnection.sharedInstance.logout(params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Toast.makeText(getApplicationContext(), "Logut Successfully", Toast.LENGTH_LONG).show();
                SharedPrefsData.clearSharedPrefernce(getApplicationContext());

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.5f);
        } else {
            v.setAlpha(1.0f);
        }
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Games Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://appslab.chinesecharacteracquisitiongame/http/host/path")
        );
        AppIndex.AppIndexApi.start(client2, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Games Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://appslab.chinesecharacteracquisitiongame/http/host/path")
        );
        AppIndex.AppIndexApi.end(client2, viewAction);
        client2.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindDrawables(findViewById(R.id.gameMainMenu_main_content));
    }

    public void unbindDrawables(View view) {//pass your parent view here
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onTrimMemory(int level) {

        // Determine which lifecycle or system event was raised.
        switch (level) {

            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                break;

            default:
                break;
        }
    }
}
