package appslab.chinesecharacteracquisitiongame.gamescene;

import android.app.Activity;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;

import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.Util.MyApplication;

public class MainActivity extends Activity implements View.OnClickListener, View.OnTouchListener {

    ImageButton login;
    ImageButton register;
    String email,password,name;
    AsyncHttpClient client = new AsyncHttpClient();
    SoundController sc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
        Log.w("Main", "on create");

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.main_main);
        //For playing sound
        sc = new SoundController();

        login = (ImageButton) findViewById(R.id.btnSubmit);
        login.setOnClickListener(this);
        login.setOnTouchListener(this);

        register = (ImageButton) findViewById(R.id.btnRegister);
        register.setOnClickListener(this);
        register.setOnTouchListener(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.w("Main", "on stop");
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState); // the UI component values are saved here.
    }


    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.getInstance().clearApplicationData();
        MyApplication.getInstance().clearApplicationData();
//        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unbindDrawables(findViewById(R.id.mainActivity_main_content));
        Log.w("Main", "on destroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.w("Main", "on restart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.w("Main", "on start");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSubmit) {
//            Toast.makeText(getApplicationContext(), "Login" ,Toast.LENGTH_LONG).show();
            Log.d("sunny", "MainActivity"+ "go to Login");
            goToNextPage("login");
        } else if (v.getId() == R.id.btnRegister) {
//          register();
            goToNextPage("register");
        }
    }

    public void goToNextPage(String action){
        Intent i;
        switch(action) {
            case "login":
                sc.playButtonSoundTrack(MainActivity.this);
                i = new Intent(getApplicationContext(),Login.class);
                startActivity(i);
                finish();
                break;
            case "register":
                sc.playButtonSoundTrack(MainActivity.this);
                i = new Intent(getApplicationContext(),Registration.class);
                startActivity(i);
                finish();
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            v.setAlpha(0.5f);
        }else{
            v.setAlpha(1.0f);
        }
        return false;
    }



    public void unbindDrawables(View view) {//pass your parent view here
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onTrimMemory(int level) {

        // Determine which lifecycle or system event was raised.
        switch (level) {

            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                break;

            default:
                break;
        }
    }

}

