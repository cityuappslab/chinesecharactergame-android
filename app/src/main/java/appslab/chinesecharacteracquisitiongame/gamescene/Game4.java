package appslab.chinesecharacteracquisitiongame.gamescene;

/*
* This comment was created by Johnny on 21/07/2016
* This activity is the game 4 (Tap the word which is equivalent to the target word)
* Theme : Octopus (Sea)
* */

import android.animation.ObjectAnimator;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.ComponentCallbacks2;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import appslab.chinesecharacteracquisitiongame.dialogs.IntroductionFragmentGame4;
import appslab.chinesecharacteracquisitiongame.dialogs.LevelCompletedFragment;
import appslab.chinesecharacteracquisitiongame.dialogs.LevelFailedFragment;
import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;
import appslab.chinesecharacteracquisitiongame.gamesetting.Game4TimeStamp;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameDataHelper;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameLevelShared;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameTrial;
import appslab.chinesecharacteracquisitiongame.networkconnection.MatrixPosition;
import cz.msebera.android.httpclient.Header;
import appslab.chinesecharacteracquisitiongame.gamesetting.Character;
import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameController;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameCoreData;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameTimer;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;
import appslab.chinesecharacteracquisitiongame.networkconnection.DeviceConnection;
import cz.msebera.android.httpclient.entity.StringEntity;

public class Game4 extends Activity implements View.OnClickListener, View.OnTouchListener, GameController, IntroductionFragmentGame4.OnItemClickedListener, LevelFailedFragment.FailFragmentOnItemClickedListener, LevelCompletedFragment.LevelCompletedFragmentOnItemClickedListener {
    Character[][] characters;
    ArrayList<String> characterArrayList = new ArrayList<String>();
    ArrayList<JSONObject> characterSetJSONObjArrayList = new ArrayList<JSONObject>();
    ArrayList<Integer> list = new ArrayList<Integer>();
    LinearLayout linear;
    ImageButton sound, ibGame4Pause, ibbacktomenu, ibresume;
    TextView editUserDisplayName;
    ProgressBar progressBar;
    ArrayList<Button> listIb = new ArrayList<Button>();
    GameTimer gameTimer;
    GameCoreData gcd;
    ObjectAnimator animation;
    Dialog pauseDialog;
    double accuracy = 0.0;
    JSONObject resultUpload;
    SoundController sc;
    ProgressDialog progressDialog;
    Random rand = new Random();
    IntroductionFragmentGame4 introductionFragmentGame4;
    FragmentManager fm = getFragmentManager();
    LevelCompletedFragment lcf;
    LevelFailedFragment lff;
    ImageView levelUpIcon;
    double textDisappearTime;
    static GameLevelShared GLS = GameLevelShared.getInstance();
    MatrixPosition matrixPosition;
    Game4TimeStamp game4TimeStamp = new Game4TimeStamp();
    boolean enableOnClickForWholeClass = false;
    boolean isTwoWrongWordsSelected = false;
    boolean isProcessGameEnd = true;


    private static final int game_id = GameDataHelper.GAME_2_ID;

    int row = 2, col = 4;
    int playingLevel;
    int stage;
    int DELAY_TO_NEXT = 2000;

    //red word
    String ans_red = "A";
    int ansrow_red = 0, anscol_red = 0;
    //another word that isn't in red
    String ans = "B";
    int ansrow = 9, anscol = 9;

//    Button btn_resume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game4_main);

        //For recording the start time of the game and submit to server afterward
        game4TimeStamp.createStartingTimeStamp();
        Log.d("Game 1 TimeStamp : ", game4TimeStamp.getStartingTimeStamp() + "");

        //Get the small data from the previous activity
        getExtras();

        DeviceConnection dc = new DeviceConnection();
        if (dc.checkNetworkAvailability(Game4.this)) {   //Check if the device connected to the Internet
            getCoreData();
        } else {
            //If there is no network, finish this activity directly
            finish();
        }
    }

    public void getCoreData() {
        gcd = new GameCoreData();

        APIConnection.sharedInstance.getGameData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String error = String.valueOf(response.getString("error"));

                    switch (error) {
                        //Fail to get game data
                        case "true":
                            Toast.makeText(getApplicationContext(), "Fail to get the Game Data. ", Toast.LENGTH_LONG).show();
                            finish();
                            break;
                        //Get game data successfully
                        case "false":
                            if (response.getJSONArray("games").getJSONObject(1).getString("error").equals("false")) { //No error in the JSONArray "Game"
                                JSONArray levels = response.getJSONArray("games").getJSONObject(1).getJSONArray("GameLevel");    //Get the JSONArray of different game levels
                                JSONArray charactersSetJSONArray = response.getJSONArray("games").getJSONObject(1).getJSONArray("characterSet");

                                Log.d("****Game 4 levels", levels.toString());

                                for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                                    characterSetJSONObjArrayList.add(charactersSetJSONArray.getJSONObject(i));
                                }

                                //This snippet is for extracting the relevant character set (Create a index for characterSet)
                                int characterSetId = GLS.getGameLevels().get(playingLevel).getCharacterSet_id();
                                int characterSetIndex = 0;
                                for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                                    if (characterSetId == Integer.parseInt(characterSetJSONObjArrayList.get(i).getString("characterSet_id"))) {
                                        characterSetIndex = i;
                                    }
                                }

                                //Set the character set 1 to the characterArrayList
                                for (int i = 0; i < (characterSetJSONObjArrayList.get(characterSetIndex).length() - 1); i++) {
                                    characterArrayList.add(characterSetJSONObjArrayList.get(characterSetIndex).getJSONObject("" + i).getString("oneCharacter"));
                                }

                                //The game flow goes here:
                                initializeUI();
                                prepareGameTrials();

                                row = levels.getJSONObject(playingLevel - 1).getInt("numOfRow");
                                col = levels.getJSONObject(playingLevel - 1).getInt("numOfCol");

                                Log.d("sunny", "***row: "+ String.valueOf(row));
                                Log.d("sunny","***col: "+ String.valueOf(col));

                                createCharacter();
                                //will start the game directly if the user set not to show again
                                showIntroductionDialog();
                            }
                            break;
                        default:
                            Toast.makeText(getApplicationContext(), "Login: UNKNOWN", Toast.LENGTH_LONG).show();
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                //When getting the data, a dialog is shown
                progressDialog = ProgressDialog.show(Game4.this, "Loading...",
                        "Please wait..", true);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                //Update the progress bar
                long progressPercentage = (long) 100 * bytesWritten / totalSize;
                progressDialog.setProgress((int) progressPercentage);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.w("Game1-onFailure", String.valueOf(statusCode));
                Log.w("Game1-onFailure", responseString);
                progressDialog.dismiss();
            }
        });
    }

    public void initializeUI() {
        findResources();
        setBtnListener();
        setUserName();
        setLVUPIcon();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindDrawables(findViewById(R.id.game4_main_content));
        int game_id = GameDataHelper.GAME_2_ID;
        Log.d("sunny", "Before: " + String.valueOf(SharedPrefsData.getGameLevel(getApplicationContext(), game_id)));
        if (GeneralUtil.DEBUG_TEST_LEVEL) {
            if (GeneralUtil.DEBUG_TEST_LEVEL) {
                int gameLevel = SharedPrefsData.getGameLevel(getApplicationContext(), game_id);
                SharedPrefsData.setGameLevel_debug(getApplicationContext(),
                        game_id,
                        gameLevel);
            }
            Log.d("sunny", "After: " + String.valueOf(SharedPrefsData.getGameLevel(getApplicationContext(), game_id)));
        }
        if (GeneralUtil.DEBUG_TEST_STAGE) {
            int stage_number = SharedPrefsData.getStageNumber(getApplicationContext());
            SharedPrefsData.setStageNumber_debug(getApplicationContext(), stage_number + 1);
        }
    }

    private void getExtras() {
        Bundle extras = getIntent().getExtras();
        playingLevel = extras.getInt("PLAYING_LEVEL");
        stage = extras.getInt("STAGE");
    }

    private void findResources() {
        ibGame4Pause = (ImageButton) findViewById(R.id.ibGame4Pause);
        progressBar = (ProgressBar) findViewById(R.id.probar4);
        linear = (LinearLayout) findViewById(R.id.linearClick);
        linear.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // True means the event is ignored by the overlayed views
                if (event.getPointerCount() > 1) {
                    linear.setClickable(false);
                    Log.d("Game4", "Multitouch detected");
                    return true;
                } else {
                    linear.setClickable(true);
                }

                return event.getPointerCount() > 1 ? true : false;
            }


        });
        editUserDisplayName = (TextView) findViewById(R.id.editUserDisplayName);
        sc = new SoundController();
//        setOnResumeBtn();
    }

    private void setBtnListener() {
        ibGame4Pause.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return TouchEvent.onTouch(v, event);
            }
        });
        ibGame4Pause.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pause();
            }
        });

    }

    private void setUserName() {
        SharedPreferences settings = getSharedPreferences("PREF", 0);
        String username = settings.getString("USER_NAME", "none");
        editUserDisplayName.setText(username);
    }

    //TODO set LVUP icon
    private void setLVUPIcon() {
        levelUpIcon = (ImageView) findViewById(R.id.levelUpIcon_game_menu);
        int stage_number = SharedPrefsData.getStageNumber(getApplicationContext());
        switch (stage_number) {
            case 0://egg
                levelUpIcon.setImageResource(R.drawable.lvup_animal1);
                break;
            case 1://cracked egg
                levelUpIcon.setImageResource(R.drawable.lvup_animal2);
                break;
            case 2://little chick
                levelUpIcon.setImageResource(R.drawable.lvup_animal3);
                break;
            case 3://mother chick
                levelUpIcon.setImageResource(R.drawable.lvup_animal4);
                break;

        }
    }

    private void prepareGameTrials() {
        gcd = new GameCoreData();

        //20 Trials per level (Set 20 trials)
        for (int i = 0; i < 20; i++) {
            GameTrial gt = new GameTrial();
            double duration = (GameLevelShared.GLArrayList.get(playingLevel-1).getDuration()) * 1000;
//            Log.d("sunny", String.valueOf(GameLevelShared.GLArrayList.get(playingLevel).getGamelevel_id()));
            gt.setDuration((duration)); //Used the duration time from server
            Log.d("sunny", "game4,  duration: "+ duration+ " , playingLevel: "+ playingLevel);
            gcd.addTrial(gt);
        }
    }

    public void startAnimateProgressBar(int rate) {
        final int frate = rate;
        runOnUiThread(new Runnable() {
            public void run() {
                animation = ObjectAnimator.ofInt(progressBar, "progress", 0);
                animation.setDuration(frate);
                animation.setInterpolator(new LinearInterpolator());
                animation.start();
            }
        });

    }

    public void setProgressBar(int rate) {
        progressBar.setProgress(rate);
    }

    //check if the word selected before processGameEnd is the target word, if not, then need to listIB.clear()
    private void listIBClearCheck(){
        boolean isContainTargetWord = false;
        for (Button ib: listIb){
            isContainTargetWord = ib.getText().equals(characters[ansrow][anscol].getOneCharacter());
            if (!isContainTargetWord ){
                listIb.clear();
            }
        }
    }

    public void processGameEnd() {
//        if(gameTimer!=null) {
//            gameTimer.stop();
//            if(gcd.getGameStatus().equals("start")) {
//                runOnUiThread(new Runnable() {
//                    public void run() {
//                        animation.cancel();
//                        uploadResult(); // generateGameReport() in the Async Task of this uploadResult()
//                    }
//
//                });
//                gcd.setGameStatus("pause");
//            }
//        }
        isProcessGameEnd = true;

        listIBClearCheck();
        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        sc.playWrongSound(Game4.this);

//                        nextTrial();
//                        listIb.clear();
                        goToNextTrial();
                    }
                });
            }
        }

        //one for uploading the red target word, another is for that not in red
        Log.d("game4Test", "processGameEnd"+"listIb.size(): "+ listIb.size());
        if (listIb.size() == 0) {
            updateNoWordIsClicked(true, ans_red, ansrow_red, anscol_red);
            updateNoWordIsClicked(false, ans, ansrow, anscol);
            indicateTargetWord(ansrow,anscol);
            indicateTargetWord(ansrow_red,anscol_red);
        } else if (listIb.size() == 1) {
            if (listIb.get(0).getBackground().equals(ContextCompat.getDrawable(this, R.drawable.g2box_target_word))) {
                updateNoWordIsClicked(false, ans_red, ansrow_red, anscol_red);
                indicateTargetWord(ansrow_red,anscol_red);
                indicateTargetWord(ansrow,anscol);

            } else {
                updateNoWordIsClicked(false, ans, ansrow, anscol);
                indicateTargetWord(ansrow,anscol);
                indicateTargetWord(ansrow_red,anscol_red);

            }
        }
        if (gcd.getGameStatus().equals("stop")) {
            return;
        }


    }

    private void goToNextTrial() {
        enableOnClickForWholeClass = false;
        gameTimer.stop();

        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                gcd.setGameStatus("start");
//                nextTrial();
//
//                Log.d("sunny", "delayed");
//
//            }
//        }, length_in_millisecond);

    }

    public void startGame() {
        if (gameTimer == null) {
            gameTimer = new GameTimer(gcd);
            gameTimer.setActivity(this);
        }
        gameTimer.start();
    }

    public void resumeGame() {
        gameTimer.resume();
    }

    public void stopGame() {
        gameTimer.stop();
    }

    public void nextTrial() {
        Log.d("game4Test", "nextTrial");
        if (gcd.getGameStatus().equals("start")) {
            animation.cancel();
            gcd.setGameStatus("pause");

            //gcd.nextTrial() checks whether next level available
            if (gcd.nextTrial()) {
                //Start the next level by stopping the timer and setting the data(duration, character, progress bar), and then restart the game
                gameTimer.updateTrial();
            } else {
                uploadResult();
//                generateGameReport(accuracy);
            }
        }
    }

    private void generateGameReport(double accuracy) {
        int correctedAccuracy = GameDataHelper.getCorrectedAccuracy(accuracy);
        boolean isWin = GameDataHelper.isWin(accuracy);
        if (!isWin) {
            showLevelFailedDialog(correctedAccuracy);
        }
        if (isWin) {
            showLevelCompletedDialog(correctedAccuracy);
        }
//        Intent i = new Intent(getApplicationContext(), GameReport.class);
//        i.putExtra("CALL_FROM", "Game4");
//        i.putExtra("LEVEL", gcd.getTrial() + 1);    //+1 because the level 0 in gcd actually represents level 1
//        accuracy = (accuracy / (gcd.getTrial() + 1)) * 100;    //Calculate the accuracy
//        i.putExtra("ACCURACY", accuracy);
//        startActivity(i);
//        finish();
    }

    public void onTrimMemory(int level) {

        // Determine which lifecycle or system event was raised.
        switch (level) {

            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                break;

            default:
                break;
        }
    }

    private void showLevelFailedDialog(int tempAccuracy) {
        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        lff = LevelFailedFragment.newInstance(tempAccuracy);
        lff.setActivity(Game4.this);
//        accuracyTV = (TextView) lff.getDialog().findViewById(R.id.accuracy);
//        accuracyTV.setText(String.valueOf(accuracy));
        lff.show(fm, "Johnny");
        sc.playLoseSoundTrack(this);
    }

    private void showLevelCompletedDialog(int tempAccuracy) {
        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        lcf = LevelCompletedFragment.newInstance(tempAccuracy);
        lcf.setActivity(Game4.this);
        lcf.show(fm, "Johnny");
        sc.playWinSoundTrack(this);
    }

    private void showIntroductionDialog() {
        if (linear!=null)
            linear.setVisibility(View.INVISIBLE);
        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        /**
         * starts the game directly if the dialog is set to never show
         */
        boolean shouldShowIntroDialog = SharedPrefsData.getShouldShowInstructDialog(getApplicationContext(),game_id);
        Log.d("Sunny", "shouldShowIntroDialog: " + String.valueOf(shouldShowIntroDialog));
        if (shouldShowIntroDialog) {
            introductionFragmentGame4 = IntroductionFragmentGame4.newInstance(game_id);
            introductionFragmentGame4.show(fm, "Introduction");
        } else {
            linear.setVisibility(View.VISIBLE);
            startGame();
        }
    }

    private void uploadResult() {
        if (resultUpload != null) {
            try {
                resultUpload.put("accuracy", GameDataHelper.getCorrectedAccuracy(accuracy))
                        .put("win", GameDataHelper.isWin(accuracy));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            StringEntity entity = new StringEntity(resultUpload.toString(), "UTF-8");

            try {
                APIConnection.sharedInstance.postArray(Game4.this, entity, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            String error = String.valueOf(response.getString("error"));
                            switch (error) {
                                case "true":   //Fail to upload result
                                    generateGameReport(accuracy);
                                    Log.d("sunny", "Game4, " + "Insert error: true ");
//                                    Toast.makeText(getApplicationContext(), "Insert error: true ", Toast.LENGTH_LONG).show();
                                    break;
                                case "false":   //Upload result successfully
                                    generateGameReport(accuracy);
                                    Log.d("sunny", "Game4, " + "Insert error: False ");
//                                    Toast.makeText(getApplicationContext(), "Insert error : False ", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                        Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Fail to upload record", Toast.LENGTH_LONG).show();
                        Log.d("sunny", "Game4, " + "onFailure without JSONARRAY");
                        progressDialog.dismiss();
                        Log.w("XXXXXStatus COde:", statusCode + "");
                        Log.w("XXXXXresponseString:", responseString + "");
                    }

                    @Override
                    public void onStart() {
                        //When uploading the data, a dialog is shown
                        progressDialog = ProgressDialog.show(Game4.this, "Uploading the result to server...",
                                "Please wait..", true);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        long progressPercentage = (long) 100 * bytesWritten / totalSize;
                        progressDialog.setProgress((int) progressPercentage);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultUpload == null) {
            //If no word is clicked
            generateGameReport(accuracy);
        }
    }

    public void createCharacter() {
        //Clear the history of generated random number
        isTwoWrongWordsSelected = false;
        isProcessGameEnd = false;
        list.clear();
        listIb.clear();
        matrixPosition = new MatrixPosition();
        matrixPosition.clearHashMap();

        //Clear the history of generated random number
//        list.clear();

        //Prepare a list in order to generate non-repeated number
        list = new ArrayList<Integer>();
        for (int a = 0; a < characterArrayList.size(); a++) {
            list.add(a);
        }

        //Matrix of the character
        characters = new Character[row][col];

        //the outside layout
        linear.removeAllViews();

        AtomicInteger playingLevel = new AtomicInteger(1);
        //the row of the words
        for (int i = 0; i < characters.length; i++) {
            LinearLayout l = new LinearLayout(this);
            l.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            l.setGravity(Gravity.CENTER);
            l.setLayoutParams(params);

            //column of each row
            for (int k = 0; k < characters[i].length; k++) {
                int numThisTime = 0;

                //Prepare the random number for creation of the words
                while (list.size() != 0) {
                    Collections.shuffle(list);
                    numThisTime = list.get(0);
                    list.remove(0);
                    break;
                }

                Character c = new Character();
                c.setOneCharacter(characterArrayList.get(numThisTime));
                TextView ib = new Button(this);
                ib.setBackground(ContextCompat.getDrawable(this, R.drawable.g2box));
                ib.setPadding(0, 0, 0, 0);

                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                int HInDp = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 50, getResources()
                                .getDisplayMetrics());
                int WInDp = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 50, getResources()
                                .getDisplayMetrics());
                p.height = HInDp;
                p.width = WInDp;
                p.setMargins(20, 15, 20, 15);
                ib.setLayoutParams(p);
                ib.requestLayout();
                ib.setOnClickListener(this);
//                ib.setOnTouchListener(this);
                ib.setText(c.getOneCharacter());
                ib.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT, 4, getResources().getDisplayMetrics()));
                ib.setTextColor(Color.BLACK);
                ib.setId(playingLevel.get());
                c.setIb(ib);
                playingLevel = new AtomicInteger(playingLevel.get() + 1);
                characters[i][k] = c;
                matrixPosition.setMatrixPosition(i, k, c);
                l.addView(ib);
            }
            linear.addView(l);
        }

        linear.setPadding(0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()), 0, 0);

        boolean isDifferentBox = false;
        Button btn = null;
        String text_onRandomBtn = null;
        int random1 = 0;
        int random2 = 0;

        while (!isDifferentBox) {
            //For setting a duplicated word in random location
            String ID = String.valueOf(rand.nextInt((characters.length * characters[0].length)) + 1);   //Randomly pick up one of the words for creating duplicated word
            int buttonResID = getResources().getIdentifier(ID, "id",
                    this.getPackageName());
            Log.d("sunny", "game4" + ", ID: " + ID);
            Log.d("sunny", "game4" + ", buttonResID: " + buttonResID);
            //Set the target word's background image as red box
            btn = (Button) linear.getRootView().findViewById(buttonResID);

            random1 = rand.nextInt(characters.length);
            random2 = rand.nextInt(characters[0].length);
            ansrow = random1;
            anscol = random2;
            ans = characters[random1][random2].getOneCharacter();
            //check if the text is set on the same button
            text_onRandomBtn = String.valueOf(btn.getText());
            if (!text_onRandomBtn.equals(ans)) {
                isDifferentBox = true;
            }
        }

        Log.d("sunny", "game4: " + "ans: " + ans + ", ansrow: " + (ansrow + 1) + ", anscol: " + (anscol + 1));


        //get the position of the target word that is not in red
        for (int i = 0; i < characters.length; i++) {
            for (int j = 0; j < characters[i].length; j++) {
                boolean isSameWord = (characters[i][j].getOneCharacter().equals(text_onRandomBtn));
                Log.d("sunny", "( " + (i + 1) + " , " + (j + 1) + " ) = " + characters[i][j].getOneCharacter()+", textOnButton: "+characters[i][j].getIb().getText() + ", ans: " + ans);
                if (isSameWord) {
                    ans_red = ans;
                    ansrow_red = i;
                    anscol_red = j;
                    Log.d("sunny", "gettingPositionOfTheTargetWordNotInRed: " + ", ans_red: " + ans_red + ", ansrow_red: " + (ansrow_red + 1) + ", anscol_red: " + (anscol_red + 1));
                }
            }
        }
        //it is a must to run the for loop first, then to setText
//        btn.setBackground(ContextCompat.getDrawable(this, R.drawable.g2box_target_word));
        btn.setText(ans);
        characters[ansrow][anscol].setOneCharacter(ans);
        characters[ansrow][anscol].getIb().setText(ans);
        characters[ansrow_red][anscol_red].setOneCharacter(ans);
        characters[ansrow_red][anscol_red].getIb().setText(ans);
        enableOnClickForWholeClass = true;
    }

    public void removeTargetWord() {
        //Do nothing
    }


    @Override
    public void onClick(View v) {
        if(enableOnClickForWholeClass) {
            Log.d("game4Test", "onClick"+"listIb.size(): "+ listIb.size());
            System.out.println(gcd.getGameStatus());
            handleIbAlphaAndAddWordToListIB((Button) v);
            if (gcd.getGameStatus().equals("stop")) {
                return;
            }

            //To update the word 1 response time
            if (listIb.size() == 1) {
                for (int i = 0; i < characters.length; i++) {
                    for (int k = 0; k < characters[i].length; k++) {
                        if (v.equals(characters[i][k].getIb())) {
                            updateWord1ResponseTime(listIb.get(0).getText().toString(), i, k, ans_red, ansrow_red, anscol_red);
                            break;
                        }
                    }
                }
            }

            if (listIb.size() == 2 && listIb.get(0).getText().toString().equals(listIb.get(1).getText().toString())) {
                enableOnClickForWholeClass = false;
                System.out.println(listIb.get(0).getText().toString() + "|" + listIb.get(1).getText().toString());
                listIb.get(0).setAlpha(1.0f);
                listIb.get(1).setAlpha(1.0f);
                for (int i = 0; i < characters.length; i++) {
                    for (int k = 0; k < characters[i].length; k++) {
                        if (v.equals(characters[i][k].getIb())) {
                            updateWord2ResponseTime(listIb.get(1).getText().toString(), i, k, 1, ans, ansrow, anscol);
                            break;
                        }
                    }
                }
                listIb.clear();
                sc.playCorrectSound(this);
                addAccuracy();
                nextTrial();
                return;
            }

            //Since the player has selected two unmatched words, the transparency effect will be cancelled
            if (listIb.size() == 2 && !(listIb.get(0).getText().toString().equals(listIb.get(1).getText().toString()))) {
                enableOnClickForWholeClass = false;
                animation.cancel();
                for (int i = 0; i < listIb.size(); i++) {
                    listIb.get(i).setAlpha(1.0f);
                }

                for (int i = 0; i < characters.length; i++) {
                    for (int k = 0; k < characters[i].length; k++) {
                        if (v.equals(characters[i][k].getIb())) {
                            updateWord2ResponseTime(listIb.get(1).getText().toString(), i, k, 0, ans, ansrow, anscol);
//                            characters[ansrow][anscol].getIb().setBackgroundColor(getResources().getColor(R.color.yellow));
//                            characters[ansrow_red][anscol_red].getIb().setBackgroundColor(getResources().getColor(R.color.yellow));
                            isTwoWrongWordsSelected = true;
                            indicateTargetWord(ansrow,anscol);
                            indicateTargetWord(ansrow_red,anscol_red);

                            break;
                        }
                    }
                }

                listIb.clear();
                sc.playWrongSound(this);
//            prepareUploadResult(characters[i][k].getOneCharacter(), i, k);
//            nextTrial();
                goToNextTrial();
            }
        }else{
            handleIbAlphaAndAddWordToListIB((Button) v);
            if (gcd.getGameStatus().equals("stop")) {
                return;
            }
            //To update the word 1 response time
            if (listIb.size() == 1) {
                for (int i = 0; i < characters.length; i++) {
                    for (int k = 0; k < characters[i].length; k++) {
                        if (v.equals(characters[i][k].getIb())) {
                            break;
                        }
                    }
                }
            }

            if (listIb.size() == 2 && listIb.get(0).getText().toString().equals(listIb.get(1).getText().toString())) {
                enableOnClickForWholeClass = false;
                System.out.println(listIb.get(0).getText().toString() + "|" + listIb.get(1).getText().toString());
                listIb.get(0).setAlpha(1.0f);
                listIb.get(1).setAlpha(1.0f);
                for (int i = 0; i < characters.length; i++) {
                    for (int k = 0; k < characters[i].length; k++) {
                        if (v.equals(characters[i][k].getIb())) {
                            isTwoWrongWordsSelected = true;
                            Log.d("sunny", String.valueOf(characters[i][k].getIb().getText()));

                            break;
                        }
                    }
                }
                listIb.clear();
                sc.playCorrectSound(this);
                gcd.setGameStatus("start");
                nextTrial();
                return;
            }
        }
    }

    private void indicateTargetWord(int ansrow, int anscol) {
        gameTimer.stop();
        final int tempAnsrow = ansrow;
        final int tempAnscol = anscol;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //stuff that updates ui
                animation.cancel();
                setIBClickable(characters,false);
                characters[tempAnsrow][tempAnscol].getIb().setBackgroundColor(getResources().getColor(R.color.yellow));

            }
        });
    }

    //set all non target word not clickable
    private void setIBClickable(Character[][] characters, boolean clickable){
        if (characters!=null) {
            Character char1 = characters[ansrow][anscol];
            Character char_red =  characters[ansrow_red][anscol_red];
            for (Character[] character : characters) {
                for (Character character1 : character) {
                    if (
                            character1.getOneCharacter().equals(char1.getOneCharacter())||
                                    character1.equals(char_red)) {
                        Log.d("sunny", character1.getOneCharacter()+", skipped");
                        continue;
                    }
                    character1.getIb().setClickable(clickable);
                }
            }
            Log.d("sunny", "ansrow: "+ ansrow+ "  anscol: "+anscol+ "  char1: "+char1.getIb().getText()+", "+char1.getOneCharacter());
            Log.d("sunny", "ansrow_red: "+ ansrow_red+ "  anscol_red: "+anscol_red);
//            char1.getIb().setClickable(true);
//            char_red.getIb().setClickable(true);
        }

    }

    private void updateWord1ResponseTime(String clickedWord1, int word1_row, int word1_col, String ans_red, int ansrow_red, int anscol_red) {
        if (resultUpload == null) {
            resultUpload = new JSONObject();
            try {
                resultUpload.put("game_id", "" + GameDataHelper.GAME_2_ID)
                        .put("gamelevel_id", GameDataHelper.getGameLevel_ID(GameDataHelper.GAME_2_ID, stage, playingLevel))
                        .put("records", new JSONArray())
                        .put("startingTimeStamp", game4TimeStamp.getStartingTimeStamp());
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", word1_row + 1)
                        .put("numOfCol", word1_col + 1)
                        .put("char", clickedWord1)
                        .put("firsttime", true)
//                                .put("char2", "")
                        .put("outcome", 0)
                        .put("ans", ans_red)
                        .put("ansrow", ansrow_red + 1)
                        .put("anscol", anscol_red + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", word1_row + 1)
                        .put("numOfCol", word1_col + 1)
                        .put("char", clickedWord1)
                        .put("firsttime", true)
//                                .put("char2", "")
                        .put("outcome", 0)
                        .put("ans", ans_red)
                        .put("ansrow", ansrow_red + 1)
                        .put("anscol", anscol_red + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);

//                resultUpload.getJSONArray("records").put(
//                        new JSONObject()
//                                .put("trial", gcd.getTrial() + 1)
//                                .put("responseTime", gameTimer.getResponseTime())
//                                .put("numOfRow", word1_row + 1)
//                                .put("numOfCol", word1_col + 1)
//                                .put("char", clickedWord1)
//                                .put("firsttime", false)
//                                .put("outcome", 1)
//                );
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.w("Game1 prepareUpload", resultUpload.toString());
        Log.d("game4Test", "updateWord1ResponseTime");
    }

    private void updateWord2ResponseTime(String clickedWord2, int word2_row, int word2_col, int isCorrect, String ans, int ansrow, int anscol) {
        if (resultUpload == null) {
            resultUpload = new JSONObject();
            try {
                resultUpload.put("game_id", "" + GameDataHelper.GAME_2_ID)
                        .put("gamelevel_id", GameDataHelper.getGameLevel_ID(GameDataHelper.GAME_2_ID, stage, playingLevel))
                        .put("records", new JSONArray())
                        .put("startingTimeStamp", game4TimeStamp.getStartingTimeStamp());
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", word2_row + 1)
                        .put("numOfCol", word2_col + 1)
                        .put("char", clickedWord2)
                        .put("firsttime", false)
//                                .put("char2", "")
                        .put("outcome", isCorrect)
                        .put("ans", ans)
                        .put("ansrow", ansrow + 1)
                        .put("anscol", anscol + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", word2_row + 1)
                        .put("numOfCol", word2_col + 1)
                        .put("char", clickedWord2)
                        .put("firsttime", false)
//                                .put("char2", "")
                        .put("outcome", isCorrect)
                        .put("ans", ans)
                        .put("ansrow", ansrow + 1)
                        .put("anscol", anscol + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.w("Game1 prepareUpload", resultUpload.toString());
        Log.d("game4Test", "updateWord2ResponseTime");
    }


    private void updateNoWordIsClicked(boolean isfirsttime, String ans, int ansrow, int anscol) {
        if (resultUpload == null) {
            resultUpload = new JSONObject();
            try {
                resultUpload.put("game_id", "" + GameDataHelper.GAME_2_ID)
                        .put("gamelevel_id", GameDataHelper.getGameLevel_ID(GameDataHelper.GAME_2_ID, stage, playingLevel))
                        .put("records", new JSONArray())
                        .put("startingTimeStamp", game4TimeStamp.getStartingTimeStamp());
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", 1)
                        .put("numOfCol", 1)
                        .put("char", "X")
                        .put("firsttime", isfirsttime)
//                                .put("char2", "")
                        .put("outcome", 0)
                        .put("ans", ans)
                        .put("ansrow", ansrow + 1)
                        .put("anscol", anscol + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", 1)
                        .put("numOfCol", 1)
                        .put("char", "X")
                        .put("firsttime", isfirsttime)
//                                .put("char2", "")
                        .put("outcome", 0)
                        .put("ans", ans)
                        .put("ansrow", ansrow + 1)
                        .put("anscol", anscol + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.w("Game1 prepareUpload", resultUpload.toString());
        Log.d("game4Test", "updateNoWordIsClicked");

    }

    private void addAccuracy() {
        accuracy = accuracy + 1;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (gcd.getGameStatus().equals("stop") && v != sound) {
            return false;
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (v instanceof Button) {
//                handleIbAlpha((Button) v);
            }
//            v.setAlpha(0.5f);
        } else {
            if (v instanceof Button) {
                return false;
            } else {
//                v.setAlpha(1.0f);
            }
        }
        return false;
    }

    public void handleIbAlphaAndAddWordToListIB(Button ib) {
        Log.d("sunny", "game4,  "+  "text on the clicked button: "+ib.getText());
        if (listIb.size() < 2 && !listIb.contains(ib)) {
            listIb.add((Button) ib);
            ib.setAlpha(0.5f);
        }
    }

    @Override
    public void onBackPressed() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (animation != null) {
                    animation.cancel();
                }
            }

        });
        gcd.setGameStatus("pause");
        ibGame4Pause.performClick();
    }

    public void pause() {
        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }

                });
                gcd.setGameStatus("pause");
            }
        }
        if (pauseDialog == null || !(pauseDialog.isShowing())) {
            //the below dialog is for pausing
            pauseDialog = new Dialog(Game4.this);
            pauseDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pauseDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pauseDialog.setContentView(R.layout.dialog_pause);
            pauseDialog.setCanceledOnTouchOutside(false);
            pauseDialog.setCancelable(false);

            //there are a lot of settings, for dialog, check them all out!

            //set up image view
            ibbacktomenu = (ImageButton) pauseDialog.findViewById(R.id.ibbacktomenu);
            ibbacktomenu.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return TouchEvent.onTouch(v, event);
                }
            });
            ibbacktomenu.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //the dialog must be cancelled before finish the activity, otherwise, an error "window leaked" will be caused
                    pauseDialog.cancel();
                    finish();
                }
            });

            ibresume = (ImageButton) pauseDialog.findViewById(R.id.ibresume);
            ibresume.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return TouchEvent.onTouch(v, event);
                }
            });
            ibresume.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pauseDialog.dismiss();
                    pauseDialog.cancel();
                    if (gcd.getGameStatus().equals("pause") && !isProcessGameEnd && !isTwoWrongWordsSelected) {
                        Log.d("sunny", "game4  "+ "game resumed");
                        gameTimer.resume();
                    }
                }
            });
            //now that the dialog is set up, it's time to show it
            pauseDialog.show();
        }
    }

    //When the introduction dialog is showing: if the user press the back pressed button
    //the game will start at this moment
    @Override
    public void onItemClicked() {
        linear.setVisibility(View.VISIBLE);
        startGame();
    }

    //When the fail Fragment dialog is showing: if the user press the back pressed button
    //the activity will finish itself and go back to game level menu.
    @Override
    public void failFragmentOnItemClicked() {
        finish();
    }

    @Override
    public void levelCompletedFragmentOnItemClicked() {
        finish();
    }


    public void unbindDrawables(View view) {//pass your parent view here
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            }else if(view instanceof TextView){
                TextView textView = (TextView) view;
                textView.setBackground(null);

            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void setOnResumeBtn(){
//        btn_resume = (Button) findViewById(R.id.btn_testResum);
//        btn_resume.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                resumeButton();
//            }
//        });
//    }
//
//    public void resumeButton(){
//        if (gcd.getGameStatus().equals("pause") && !isProcessGameEnd && !isTwoWrongWordsSelected) {
//            Log.d("sunny", "game4  "+ "game resumed");
//            gameTimer.resume();
//        }
//    }

}
