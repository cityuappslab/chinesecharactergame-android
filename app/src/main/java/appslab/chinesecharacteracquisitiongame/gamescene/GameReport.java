package appslab.chinesecharacteracquisitiongame.gamescene;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import appslab.chinesecharacteracquisitiongame.R;

public class GameReport extends Activity {
    TextView levelAttainedTV, accuracyTV;
    int levelAttained;
    double accuracy;
    ImageButton ibGamePause;
    TextView editUserDisplayName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String callFrom = extras.getString("CALL_FROM");
            levelAttained = extras.getInt("LEVEL");
            accuracy = Math.round(extras.getDouble("ACCURACY"));

            //Game 1 Report
            if (callFrom.equals("Game1")) {
                setContentView(R.layout.game_report_game1);
                findResources();
                setBtnListener();
                fillTextView();
                setUserName();
            }

            //Game 4 Report
            if (callFrom.equals("Game4")) {
                setContentView(R.layout.game_report_game4);
                findResources();
                setBtnListener();
                fillTextView();
                setUserName();
            }

            //Game 5 Bird Report
            if (callFrom.equals("Game5_Bird")) {
                setContentView(R.layout.game_report_game5_bird);
                findResources();
                setBtnListener();
                fillTextView();
                setUserName();
            }

            //Game 5 Bat Report
            if (callFrom.equals("Game5_Bat")) {
                setContentView(R.layout.game_report_game5_bat);
                findResources();
                setBtnListener();
                fillTextView();
                setUserName();
            }
        } else {
            Log.w("GAMEREPORT: ", "extra is null!!!");
        }
    }


    private void findResources() {
        ibGamePause = (ImageButton) findViewById(R.id.ibGamePause);
        levelAttainedTV = (TextView) findViewById(R.id.levelAttained);
        accuracyTV = (TextView) findViewById(R.id.accuracy);
        editUserDisplayName = (TextView) findViewById(R.id.editUserDisplayName);
    }


    private void setBtnListener() {
        ibGamePause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Games.class);
                startActivity(intent);
            }
        });
    }

    private void fillTextView() {
        levelAttainedTV.setText("Level: " + levelAttained);
        accuracyTV.setText("Accuracy: " + accuracy + "%");
    }

    private void setUserName() {
        SharedPreferences settings = getSharedPreferences("PREF", 0);
        String username = settings.getString("USER_NAME", "none");
        editUserDisplayName.setText(username);
    }
}