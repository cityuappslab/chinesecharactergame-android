package appslab.chinesecharacteracquisitiongame.gamescene;

/*
* This comment was created by Johnny on 21/07/2016
* This activity is the game 1 (Tap the word which is equivalent to the target word)
* Theme : Human
* */

import android.animation.ObjectAnimator;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.dialogs.IntroductionFragment;
import appslab.chinesecharacteracquisitiongame.dialogs.LevelCompletedFragment;
import appslab.chinesecharacteracquisitiongame.dialogs.LevelFailedFragment;
import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;
import appslab.chinesecharacteracquisitiongame.gamesetting.Game1TimeStamp;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameDataHelper;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameLevelShared;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameTrial;
import appslab.chinesecharacteracquisitiongame.gamesetting.WordTimer;
import appslab.chinesecharacteracquisitiongame.networkconnection.MatrixPosition;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import appslab.chinesecharacteracquisitiongame.gamesetting.Character;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameController;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameCoreData;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameTimer;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;
import appslab.chinesecharacteracquisitiongame.networkconnection.DeviceConnection;

public class Game1 extends Activity implements View.OnClickListener, View.OnTouchListener, GameController, IntroductionFragment.OnItemClickedListener, LevelFailedFragment.FailFragmentOnItemClickedListener, LevelCompletedFragment.LevelCompletedFragmentOnItemClickedListener, ComponentCallbacks2 {
    private static final int game_id = GameDataHelper.GAME_1_ID;
    static GameLevelShared GLS = GameLevelShared.getInstance();
    Character[][] characters;
    ArrayList<String> characterArrayList = new ArrayList<String>();
    ArrayList<JSONObject> characterSetJSONObjArrayList = new ArrayList<JSONObject>();
    ArrayList<Integer> list = new ArrayList<Integer>();
    LinearLayout linear;
    ImageButton sound, ibGame1Pause, ibbacktomenu, ibresume;
    TextView editUserDisplayName;
    ProgressBar progressBar;
    TextView clickButton;
    GameTimer gameTimer;
    WordTimer wordTimer;
    GameCoreData gcd;
    ObjectAnimator animation;
    Dialog pauseDialog, levelCompletedDialog;
    LevelCompletedFragment lcf;
    LevelFailedFragment lff;
    IntroductionFragment introductionFragment;
    double accuracy = 0.0;
    JSONObject resultUpload = null;
    SoundController sc;
    ProgressDialog progressDialog;
    double textDisappearTime;
    Random rand = new Random();
    FragmentManager fm = getFragmentManager();
    LinearLayout targetWord;
    Thread thread;
    Runnable mHandlerTask;
    TextView accuracyTV;
    ImageView ivBackgroud;
    MatrixPosition matrixPosition;
    Game1TimeStamp game1TimeStamp = new Game1TimeStamp();
    boolean isOnClickForWholeClass = false;
    boolean isProcessGameEnd = true;
    //for fixing the resume problem when resume button is clicked after wrong word is chosen
    boolean isWrongWordSelected = true;

    int playingLevel;
    int row = 4, col = 5;

    String ans = "A";
    int ansrow = 0, anscol = 0;

    ImageView levelUpIcon;
    int stage;
    int DELAY_TO_NEXT = 2000;
//    private IndexProcessor runnable = null;

    public void onTrimMemory(int level) {

        // Determine which lifecycle or system event was raised.
        switch (level) {

            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                break;

            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game1_main);

        //For recording the start time of the game and submit to server afterward
        game1TimeStamp.createStartingTimeStamp();
        Log.d("Game 1 TimeStamp : ", game1TimeStamp.getStartingTimeStamp() + "");

        //Get the small data from the previous activity
        getExtras();

        //Check if the device is connected to the Internet
        DeviceConnection dc = new DeviceConnection();
        if (dc.checkNetworkAvailability(Game1.this)) {
            //Get the data from the server (e.g. Characters)
            //After the data is retrieved, the game flow will be started
            getCoreData();
        } else {
            //If the device is not connecting to the Internet
            //The activity will finish itself swiftly
            finish();
        }
    }

    public void initializeUI() {
//        getExtras();
        findResources();
        setBtnListener();
        setUserName();
        //TODO set lvUp icon
        setLVUPIcon();

    }

    public void getCoreData() {
        APIConnection.sharedInstance.getGameData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    String error = String.valueOf(response.getString("error"));

                    //Fail to get game data
                    switch (error) {
                        case "true":
                            Toast.makeText(getApplicationContext(), "Fail to get the Game Data. ", Toast.LENGTH_LONG).show();
                            finish();
                            break;
                        //Get Game Data successfully
                        case "false":
                            initializeUI();
                            /** set the words invisible before the dialog closed*/
                            linear.setVisibility(View.INVISIBLE);


                            //No error in the JSONArray "Game"
                            if (response.getJSONArray("games").getJSONObject(0).getString("error").equals("false")) {

                                //Get the characterSet as the format of JSONArray
                                JSONArray charactersSetJSONArray = response.getJSONArray("games").getJSONObject(0).getJSONArray("characterSet");

                                //Get num of row and col
                                JSONArray levels = response.getJSONArray("games").getJSONObject(0).getJSONArray("GameLevel");

                                //Fetch the objects of characterSet in the JSONArray
                                for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                                    characterSetJSONObjArrayList.add(charactersSetJSONArray.getJSONObject(i));
                                }

                                //This snippet is for extracting the relevant character set (Create a index for characterSet)
                                int characterSetId = GLS.getGameLevels().get(playingLevel).getCharacterSet_id();
                                int characterSetIndex = 0;
                                for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                                    if (characterSetId == Integer.parseInt(characterSetJSONObjArrayList.get(i).getString("characterSet_id"))) {
                                        characterSetIndex = i;
                                    }
                                }

                                //Set the character set 1 to the characterArrayList
                                for (int i = 0; i < (characterSetJSONObjArrayList.get(characterSetIndex).length() - 1); i++) {
                                    characterArrayList.add(characterSetJSONObjArrayList.get(characterSetIndex).getJSONObject("" + i).getString("oneCharacter"));
                                }

                                //The game flow goes here:

//                                linear.setVisibility(View.VISIBLE);
                                prepareGameTrials();

                                row = levels.getJSONObject(playingLevel - 1).getInt("numOfRow");
                                col = levels.getJSONObject(playingLevel - 1).getInt("numOfCol");
                                Log.d("sunny", "game1, "+ "playingLevel: "+ playingLevel);
                                Log.d("sunny", "game1, "+ "levelsJSONArray: "+ levels);
                                Log.d("Game1", "sunny " + "row: " + row + "," + "col: " + col);


                                //will start the game directly if the user set not to show again
                                createCharacter();
                                showIntroductionDialog();
                            }
                            break;
                        default:
                            Toast.makeText(getApplicationContext(), "Login: UNKNOWN", Toast.LENGTH_LONG).show();
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                //When getting the data, a dialog is shown
                progressDialog = ProgressDialog.show(Game1.this, "Loading...",
                        "Please wait..", true);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                //Update the progress bar
                long progressPercentage = (long) 100 * bytesWritten / totalSize;
                progressDialog.setProgress((int) progressPercentage);
            }

            @Override
            public void onFinish() {
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.d("Game1-onFailure", "Game1: " + "onFailure without JSONARRAY");
                Log.w("Game1-onFailure", String.valueOf(statusCode));
                Log.w("Game1-onFailure", responseString);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindDrawables(findViewById(R.id.bg_layer2));
        Log.v("samson","reached");
        int game_id = GameDataHelper.GAME_1_ID;
        Log.d("sunny", "Before: " + String.valueOf(SharedPrefsData.getGameLevel(getApplicationContext(), game_id)));
        if (GeneralUtil.DEBUG_TEST_LEVEL) {
            if (GeneralUtil.DEBUG_TEST_LEVEL) {
                int gameLevel = SharedPrefsData.getGameLevel(getApplicationContext(), game_id);
                SharedPrefsData.setGameLevel_debug(getApplicationContext(),
                        game_id,
                        gameLevel);
            }
            Log.d("sunny", "After: " + String.valueOf(SharedPrefsData.getGameLevel(getApplicationContext(), game_id)));
        }
        int stage_number = SharedPrefsData.getStageNumber(getApplicationContext());
        if (GeneralUtil.DEBUG_TEST_STAGE) {
            SharedPrefsData.setStageNumber_debug(getApplicationContext(), stage_number + 1);
        }

    }

    private void getExtras() {
        Bundle extras = getIntent().getExtras();
        playingLevel = extras.getInt("PLAYING_LEVEL");
        stage = extras.getInt("STAGE");
        textDisappearTime = GLS.getGameLevels().get(playingLevel).getText_disappear_time();
        Log.d("Game1 Disappear", textDisappearTime + "");
    }

    private void findResources() {
        ibGame1Pause = (ImageButton) findViewById(R.id.ibGame1Pause);
        progressBar = (ProgressBar) findViewById(R.id.probar1);
        linear = (LinearLayout) findViewById(R.id.linearClick);
        editUserDisplayName = (TextView) findViewById(R.id.editUserDisplayName);
//        ivBackgroud = (ImageView) findViewById(R.id.iv_background);
//        int resizeFactor = 5;
//        int width = 1280/resizeFactor;
//        int height = 720/resizeFactor;
//        Glide.with(this)
//                .load("")
//                .placeholder(R.drawable.g1s)
//                .override(width,height)
//                .into(ivBackgroud);

//        Picasso.with(this)
//                .load("")
//                .into(ivBackgroud);


        sc = new SoundController();
    }

    private void setLVUPIcon() {
        levelUpIcon = (ImageView) findViewById(R.id.levelUpIcon_game_menu);
        int stage_number = SharedPrefsData.getStageNumber(getApplicationContext());
        switch (stage_number) {
            case 0://egg
                levelUpIcon.setImageResource(R.drawable.lvup_animal1);
                break;
            case 1://cracked egg
                levelUpIcon.setImageResource(R.drawable.lvup_animal2);
                break;
            case 2://little chick
                levelUpIcon.setImageResource(R.drawable.lvup_animal3);
                break;
            case 3://mother chick
                levelUpIcon.setImageResource(R.drawable.lvup_animal4);
                break;

        }
    }

    private void setBtnListener() {
        ibGame1Pause.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return TouchEvent.onTouch(v, event);
            }
        });
        ibGame1Pause.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pause();
            }
        });


    }

    private void setUserName() {
        SharedPreferences settings = getSharedPreferences("PREF", 0);
        String username = settings.getString("USER_NAME", "none");
        editUserDisplayName.setText(username);
    }

    private void prepareGameTrials() {
        gcd = new GameCoreData();

        //20 Trials per level (Set 20 trials)
        for (int i = 0; i < 20; i++) {
            GameTrial gt = new GameTrial();
            //TODO uncomment to use the server's duration
            gt.setDuration(((GameLevelShared.GLArrayList.get(playingLevel).getDuration()) * 1000)); //Used the duration time from server
//            gt.setDuration(5000); //Used the duration time from server
            gcd.addTrial(gt);
        }
    }

    public void startAnimateProgressBar(int rate) {
        final int frate = rate;
        runOnUiThread(new Runnable() {
            public void run() {
                animation = ObjectAnimator.ofInt(progressBar, "progress", 0);
                animation.setDuration(frate);
                animation.setInterpolator(new LinearInterpolator());
                animation.start();
//                Log.d("timer", "Game1 progress bar: " +frate + "\tcurrent system time: "+ System.currentTimeMillis());
            }
        });
    }

    public void setProgressBar(int rate) {
        progressBar.setProgress(rate);
    }

    public void processGameEnd() {
        isOnClickForWholeClass = false;
        isProcessGameEnd = true;
        setImageButtonsClickable(characters, false);
        prepareUploadEmptyResult(ans, ansrow, anscol);
        wordTimer.stop();
        if (gcd.getGameStatus().equals("stop")) {
            Log.d("sunny", "(gcd.getGameStatus().equals(\"stop\")");
            return;
        }

        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
//                        animation.cancel();
//                        generateGameReport(accuracy);
//                        uploadResult(); // generateGameReport() in the Async Task of this uploadResult()

                        sc.playWrongSound(Game1.this);
                        // The positions are filled with 0, but actually it is null eventually upload to server.

                        //TODO add wait
                        goToNextTrial();
//                            nextTrial();
                    }
                });
            }
        }
    }

    private void setImageButtonsClickable(Character[][] characters, boolean clickable) {
        if (characters != null) {
//            for (Character[] characters1 : characters) {
//                for (Character character : characters1) {
//                    TextView textView = character.getIb();
//                    if (clickable){
//                        textView.setOnClickListener(this);
//                    }
//                    textView.setClickable(clickable);
//
//
//
//                }
//            }
        }
        Log.d("sunny", "null characters"+ "clickable: "+ clickable);
    }

    //TODO goToNextTrial
    private void goToNextTrial() {
        wordTimer.stop();

        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }


//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                gcd.setGameStatus("start");
//                nextTrial();
//
//                Log.d("sunny", "delayed");
//
//            }
//        }, length_in_millisecond);
    }

    public void startGame() {
        if (gameTimer == null) {
            gameTimer = new GameTimer(gcd);
            gameTimer.setActivity(this);
        }

        //Create a timer to wait 3 seconds then remove the target word
        wordTimer = new WordTimer(gameTimer, textDisappearTime * 1000);
        wordTimer.setActivity(this);
        gameTimer.start();
        wordTimer.start();
    }

    public void restart() {
        gameTimer = new GameTimer(gcd);
        gameTimer.setActivity(this);


    }

    public void resumeGame() {
        gameTimer.resume();
    }

    public void stopGame() {
        gameTimer.stop();
    }


    //TODO
    public void nextTrial() {
        isOnClickForWholeClass = false;
        if (gcd.getGameStatus().equals("start")) {
            animation.cancel();
            if (gcd.nextTrial()) {
                gameTimer.updateTrial();
                Log.d("sunny", "update trial");
                setImageButtonsClickable(characters, true);
            } else {
                //When the player loses or all the trials are finished, a report will be generated.
//                generateGameReport(accuracy);
                setImageButtonsClickable(characters,false);
                uploadResult();
            }
        }
    }

    private void generateGameReport(double accuracy) {

        int correctedAccuracy = GameDataHelper.getCorrectedAccuracy(accuracy);
        boolean isWin = GameDataHelper.isWin(accuracy);
        if (!isWin) {
            showLevelFailedDialog(correctedAccuracy);
        }
        if (isWin) {
            showLevelCompletedDialog(correctedAccuracy);
        }
    }

    private void showLevelFailedDialog(int tempAccuracy) {
        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }


        lff = LevelFailedFragment.newInstance(tempAccuracy);
        lff.setActivity(Game1.this);
        lff.show(fm, "Johnny");
        sc.playLoseSoundTrack(this);
    }


    private void showIntroductionDialog() {
        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        /**
         * starts the game directly if the dialog is set to never show
         */
        boolean shouldShowIntroDialog = SharedPrefsData.getShouldShowInstructDialog(getApplicationContext(), game_id);
        Log.d("Sunny", "shouldShowIntroDialog: " + String.valueOf(shouldShowIntroDialog));
        if (shouldShowIntroDialog) {
            if (linear!=null)
                linear.setVisibility(View.INVISIBLE);
            introductionFragment = IntroductionFragment.newInstance(game_id);
            introductionFragment.show(fm, "Introduction");
        } else {
            if(linear!=null)
                linear.setVisibility(View.VISIBLE);
            startGame();

        }
    }

    private void showLevelCompletedDialog(int tempAccuracy) {
        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        lcf = LevelCompletedFragment.newInstance(tempAccuracy);
        lcf.setActivity(Game1.this);
        lcf.show(fm, "Johnny");
        sc.playWinSoundTrack(this);
    }

    private void uploadResult() {
        if (resultUpload != null) {
            try {
                resultUpload.put("accuracy", GameDataHelper.getCorrectedAccuracy(accuracy))
                        .put("win", GameDataHelper.isWin(accuracy));
                Log.d("sunny", resultUpload.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            StringEntity entity = new StringEntity(resultUpload.toString(), "UTF-8");

            try {
                APIConnection.sharedInstance.postArray(Game1.this, entity, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            Log.d("UploadResult", String.valueOf(response));
                            String error = String.valueOf(response.getString("error"));

                            switch (error) {
                                case "true":   //Fail to upload result
                                    generateGameReport(accuracy);
                                    Log.d("sunny", "Game1, " + "Insert error: true ");
//                                    Toast.makeText(getApplicationContext(), "Insert error: true ", Toast.LENGTH_LONG).show();
                                    break;
                                case "false":   //Upload result successfully
                                    generateGameReport(accuracy);
                                    Log.d("sunny", "Game1, " + "Insert error: False ");
//                                    Toast.makeText(getApplicationContext(), "Insert error : False ", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toast.makeText(getApplicationContext(), "Fail to upload record", Toast.LENGTH_LONG).show();
                        Log.d("sunny", "Game1, " + "onFailure without JSONARRAY");
                        progressDialog.dismiss();
                        Log.w("Status Code:", statusCode + "");
                        finish();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onStart() {
                        //When uploading the data, a dialog is shown
                        progressDialog = ProgressDialog.show(Game1.this, "Uploading the result to server...",
                                "Please wait..", true);
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        long progressPercentage = (long) 100 * bytesWritten / totalSize;
                        progressDialog.setProgress((int) progressPercentage);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultUpload == null) {
            //If no word is clicked
            generateGameReport(accuracy);
        }
    }

    public void createCharacter() {
        isWrongWordSelected = false;
        isOnClickForWholeClass = false;
        isProcessGameEnd = false;
        //Clear the history of generated random number
        list.clear();
        matrixPosition = new MatrixPosition();
        matrixPosition.clearHashMap();

        //Prepare a list in order to generate non-repeated number
        list = new ArrayList<Integer>();
        for (int a = 0; a < characterArrayList.size(); a++) {
            list.add(a);
        }

        //Matrix of the character
        characters = new Character[row][col];
//        Log.d("TAG", )

        //the outside layout
        linear.removeAllViews();
        //the row of the words
        for (int i = 0; i < characters.length; i++) {
            LinearLayout l = new LinearLayout(this);
            l.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            l.setGravity(Gravity.CENTER_HORIZONTAL);
            l.setLayoutParams(params);

            //column of each row
            for (int k = 0; k < characters[i].length; k++) {
                int numThisTime = 0;

                //Prepare the random number for creation of the words
                while (list.size() != 0) {
                    Collections.shuffle(list);
                    numThisTime = list.get(0);
                    list.remove(0);
                    break;
                }

                Character c = new Character();
                c.setOneCharacter(characterArrayList.get(numThisTime));

                TextView ib = new TextView(this);
                ib.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                ib.setBackground(ContextCompat.getDrawable(this, R.drawable.square));
//                Glide.with(Game1.this)
//                        .load("")
//                        .placeholder(ContextCompat.getDrawable(this, R.drawable.square))
//                        .into(ib);
                ib.setPadding(0, 0, 0, 0);

                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                int HInDp = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 40, getResources()
                                .getDisplayMetrics());
                int WInDp = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 40, getResources()
                                .getDisplayMetrics());
                p.height = HInDp;
                p.width = WInDp;
                p.setMargins(10, 10, 10, 10);
                ib.setLayoutParams(p);
                ib.requestLayout();
                //TODO adding
                ib.setClickable(false);
                ib.setOnClickListener(this);
//                ib.setOnTouchListener(this);
                ib.setText(c.getOneCharacter());
                ib.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT, 4, getResources().getDisplayMetrics()));
                ib.setTextColor(Color.BLACK);
                c.setIb(ib);
                characters[i][k] = c;
                //insert the position of the words into the map(matrixPosition)
                matrixPosition.setMatrixPosition(i, k, c);
                l.addView(ib);
            }
            linear.addView(l);
        }

        Log.d("Sunny", matrixPosition.getJsonObject().toString());

        //adding the target word
        addTargetWord();
//        removeTargetWord();
        linear.setPadding(0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()), 0, 0);
        isOnClickForWholeClass =true;

    }

    private void addTargetWord() {
        targetWord = new LinearLayout(this);
        targetWord.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        targetWord.setGravity(Gravity.CENTER_HORIZONTAL);
        targetWord.setLayoutParams(params);
        clickButton = new TextView(this);
        clickButton.setBackground(ContextCompat.getDrawable(this, R.drawable.target_game1));

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);


        int HInDp = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 40, getResources()
                        .getDisplayMetrics());
        int WInDp = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 40, getResources()
                        .getDisplayMetrics());
        p.height = HInDp;
        p.width = WInDp;
        p.setMargins(10, 10, 10, 10);
        clickButton.setLayoutParams(p);
        clickButton.requestLayout();
        clickButton.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        clickButton.setOnClickListener(this);
//        clickButton.setOnTouchListener(this);

        ansrow = (int) Math.round(rand.nextInt(characters.length));
        anscol = (int) Math.round(rand.nextInt(characters[0].length));
        ans = characters[ansrow][anscol].getOneCharacter();

        Log.d("sunny game1", "ans: " + ans + ", (" + ansrow + "," + anscol + ")");

        clickButton.setText(ans);
        clickButton.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT, 4, getResources().getDisplayMetrics()));

        clickButton.setTextColor(Color.BLACK);
        targetWord.addView(clickButton);
        linear.addView(targetWord);
    }

    public void removeTargetWord() {
        runOnUiThread(new Runnable() {
            public void run() {
//                linear.removeView(targetWord);
                targetWord.setVisibility(View.INVISIBLE);
//                linear.setPadding(0, 0, 0, 0);
            }
        });
    }

    private void handleIbAlphaAndSetClickable(TextView ib) {
        ib.setAlpha(0.5f);
        setImageButtonsClickable(characters, false);
    }

    @Override
    public void onClick(View v) {
        if (isOnClickForWholeClass) {
            //Stop thread which is used to disappear the target word.
//        thread.stop();
            wordTimer.stop();
            if (gcd.getGameStatus().equals("stop")) {
                return;
            }

            for (int i = 0; i < characters.length; i++) {
                for (int k = 0; k < characters[i].length; k++) {
                    //Chosen the correct word, then add accuracy and next level
                    if (v.equals(characters[i][k].getIb()) && characters[i][k].getOneCharacter().equals(String.valueOf(clickButton.getText().toString()))) {
                        isOnClickForWholeClass = false;
                        handleIbAlphaAndSetClickable((TextView) v);
//                        v.setClickable(false);
                        sc.playCorrectSound(this);
                        addAccuracy();
                        prepareUploadResult(characters[i][k].getOneCharacter(), i, k, 1, ans, ansrow, anscol);
                        characters[ansrow][anscol].getIb().setBackgroundColor(getResources().getColor(R.color.yellow));
                        Log.d("sunny", "correctly selected");
                        nextTrial();
                    }

                    //Chosen wrong word
                    if (v.equals(characters[i][k].getIb()) && !(characters[i][k].getOneCharacter().equals(String.valueOf(clickButton.getText().toString())))) {
                        isOnClickForWholeClass = false;
                        isWrongWordSelected = true;
                        v.setClickable(false);
                        //System.out.println(characters[i][k].getOneCharacter());
                        handleIbAlphaAndSetClickable((TextView) v);
                        sc.playWrongSound(this);
                        prepareUploadResult(characters[i][k].getOneCharacter(), i, k, 0, ans, ansrow, anscol);
                        characters[ansrow][anscol].getIb().setBackgroundColor(getResources().getColor(R.color.yellow));
                        animation.cancel();
                        goToNextTrial();
                    }
                }
            }
        }else{
            for (int i = 0; i < characters.length; i++) {
                for (int k = 0; k < characters[i].length; k++) {
                    //Chosen the correct word, then add accuracy and next level
                    if (v.equals(characters[i][k].getIb()) && characters[i][k].getOneCharacter().equals(String.valueOf(clickButton.getText().toString()))) {
                        isOnClickForWholeClass = false;
                        handleIbAlphaAndSetClickable((TextView) v);
                        v.setClickable(false);
                        sc.playCorrectSound(this);
                        gcd.setGameStatus("start");
                        nextTrial();
                        Log.d("sunny", "check point onClick");

                    }
                }
            }
        }

    }

    private void prepareUploadResult(String clickedWord, int row, int col, int isCorrect, String ans, int ansrow, int anscol) {
        Log.d("Game1 matrixPosition", matrixPosition.getJsonObject().toString());
        if (resultUpload == null) {
            resultUpload = new JSONObject();
            try {
                resultUpload.put("game_id", "" + GameDataHelper.GAME_1_ID)
                        .put("gamelevel_id", GameDataHelper.getGameLevel_ID(GameDataHelper.GAME_1_ID, stage, playingLevel))
                        .put("records", new JSONArray())
                        .put("startingTimeStamp", game1TimeStamp.getStartingTimeStamp());
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", row + 1)
                        .put("numOfCol", col + 1)
                        .put("char", clickedWord)
                        .put("firsttime", true)
//                                .put("char2", "")
                        .put("outcome", isCorrect)
                        .put("ans", ans)
                        .put("ansrow", ansrow + 1)
                        .put("anscol", anscol + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", row + 1)
                        .put("numOfCol", col + 1)
                        .put("char", clickedWord)
                        .put("firsttime", true)
//                                .put("char2", "")
                        .put("outcome", isCorrect)
                        .put("ans", ans)
                        .put("ansrow", ansrow + 1)
                        .put("anscol", anscol + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.w("Game1 prepareUpload", resultUpload.toString());

    }

    private void prepareUploadEmptyResult(String ans, int ansrow, int anscol) {
        Log.d("Game1 matrixPosition", matrixPosition.getJsonObject().toString());
        if (resultUpload == null) {
            resultUpload = new JSONObject();
            try {
                resultUpload.put("game_id", "" + GameDataHelper.GAME_1_ID)
                        .put("gamelevel_id", GameDataHelper.getGameLevel_ID(GameDataHelper.GAME_1_ID, stage, playingLevel))
                        .put("records", new JSONArray())
                        .put("startingTimeStamp", game1TimeStamp.getStartingTimeStamp());
                ;
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", "")
                        .put("numOfCol", "")
                        .put("char", "X")
                        .put("firsttime", true)
//                                .put("char2", "")
                        .put("outcome", 0)
                        .put("ans", ans)
                        .put("ansrow", ansrow + 1)
                        .put("anscol", anscol + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", gameTimer.getResponseTime())
                        .put("numOfRow", "")
                        .put("numOfCol", "")
                        .put("char", "X")
                        .put("firsttime", true)
//                                .put("char2", "")
                        .put("outcome", 0)
                        .put("ans", ans)
                        .put("ansrow", ansrow + 1)
                        .put("anscol", anscol + 1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1, matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final int tempAnsrow = ansrow;
        final int tempAnscol = anscol;

        Log.w("Game1 prepareUpload", resultUpload.toString());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //stuff that updates ui
                characters[tempAnsrow][tempAnscol].getIb().setBackgroundColor(getResources().getColor(R.color.yellow));
            }
        });


    }

    private void addAccuracy() {
        accuracy = accuracy + 1;
//        Toast.makeText(getApplicationContext(), ""+accuracy, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (gcd.getGameStatus().equals("stop") && v != sound) {
            return false;
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            v.setAlpha(0.5f);
        } else {
            //set the image button as non-transparent
            v.setAlpha(1.0f);
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (animation != null) {
                    animation.cancel();
                }
            }

        });

        if (gcd != null)
            gcd.setGameStatus("pause");

        ibGame1Pause.performClick();
    }


    private void pause() {
        wordTimer.stop();

        if (gameTimer != null) {
            gameTimer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        if (pauseDialog == null || !(pauseDialog.isShowing())) {
            //the below dialog is for pausing
            pauseDialog = new Dialog(Game1.this);
            pauseDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pauseDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pauseDialog.setContentView(R.layout.dialog_pause);
            pauseDialog.setCanceledOnTouchOutside(false);
            pauseDialog.setCancelable(false);

            //there are a lot of settings, for dialog, check them all out!

            //set up image view
            ibbacktomenu = (ImageButton) pauseDialog.findViewById(R.id.ibbacktomenu);
            ibbacktomenu.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return TouchEvent.onTouch(v, event);
                }
            });
            ibbacktomenu.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //the dialog must be cancelled before finish the activity, otherwise, an error "window leaked" will be caused
                    pauseDialog.cancel();
                    Intent intent = new Intent(Game1.this, Game1LevelMenu.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            });

            ibresume = (ImageButton) pauseDialog.findViewById(R.id.ibresume);
            ibresume.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return TouchEvent.onTouch(v, event);
                }
            });
            ibresume.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pauseDialog.dismiss();
                    pauseDialog.cancel();
                    if (gcd.getGameStatus().equals("pause")&& !isProcessGameEnd && !isWrongWordSelected) {
                        Log.d("sunny", "game1  "+ "game resumed");
                        gameTimer.resume();
//                        mHandler.postDelayed(mHandlerTask, 3000);
                        wordTimer.resume();
                    }
                }
            });

            //now that the dialog is set up, it's time to show it
            pauseDialog.show();
        }
    }

    //start game if the close button in the fragment is pressed
    @Override
    public void onItemClicked() {
        /** for fixing the early showing of the words*/
        linear.setVisibility(View.VISIBLE);
        startGame();
    }

    //When the fail Fragment dialog is showing: if the user press the back pressed button
    //the activity will finish itself and go back to game level menu.
    @Override
    public void failFragmentOnItemClicked() {
        Intent intent = new Intent(Game1.this, Game1LevelMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void levelCompletedFragmentOnItemClicked() {
        Intent intent = new Intent(Game1.this, Game1LevelMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }



    public void unbindDrawables(View view) {//pass your parent view here
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            } else if(view instanceof TextView){
                Log.d("Game1", "Clear TextView drawable");
                TextView textView = (TextView) view;
                textView.setBackground(null);
                System.gc();


            }else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}