package appslab.chinesecharacteracquisitiongame.gamescene;

/*
* This comment was created by Johnny on 08/08/2016
* This Activity is for initializing the game data (Mainly for Audio files) at the beginning,
* but it is also possible to download the other files.
* The screen of this activity shows a progress bar with % in order to show the progress to the user.
* */

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.utils.NetworkUtils;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;
import cz.msebera.android.httpclient.Header;

public class GameLoadFiles extends AppCompatActivity {
    public static final int progress_bar_type = 0;
    private static final String TAG = "GameLoadFiles";
    final File newFolder = new File("/sdcard/chinesegamecharactergame_audios");
    ProgressBar pb;
    ArrayList<JSONObject> characterSetJSONObjArrayList = new ArrayList<JSONObject>();
    ArrayList<String> characterAudioArrayList = new ArrayList<String>();
    ArrayList<URL> URLArrayList = new ArrayList<URL>();
    URL[] urls = null;
    //A regular expression to determine the audio file name (Include the extension name) in a URL
    Pattern pattern = Pattern.compile("http:\\/\\/.*\\/character\\/audio\\/(.*)");
    Matcher matcher;
    ArrayList<String> audioFileName = new ArrayList<>();
    TextView progressIndicator;
    private DownloadFilesTask dft;

    //For the purpose of debug testing
    public static void longInfo(String str) {
        if (str.length() > 4000) {
            Log.i(TAG, str.substring(0, 4000));
            longInfo(str.substring(4000));
        } else
            Log.i(TAG, str);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_load_files);
        pb = (ProgressBar) findViewById(R.id.progressBar);
        progressIndicator = (TextView) findViewById(R.id.progressIndicator);

        if(!NetworkUtils.isConnected(this)){
//            Toast.makeText(this, "Please enable network connection to procceed", Toast.LENGTH_SHORT).show();
            GeneralUtil.showAlertDialog_NetworkConnection(this, "Please enable network connection to procceed");

        }

        hideStatusBar();

        APIConnection.sharedInstance.getGameData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                JSONObject obj = response;

                try {
                    /************ Start : For adding the character set into json arraylist************/

                    //For Game 1
                    if (obj.getJSONArray("games").getJSONObject(0).getString("error") == "false") { //No error in the JSONArray "Game"
                        JSONArray charactersSetJSONArray = obj.getJSONArray("games").getJSONObject(0).getJSONArray("characterSet");

                        for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                            characterSetJSONObjArrayList.add(charactersSetJSONArray.getJSONObject(i));
                        }
                    }

                    //For Game 2
                    if (obj.getJSONArray("games").getJSONObject(1).getString("error") == "false") { //No error in the JSONArray "Game"
                        JSONArray charactersSetJSONArray = obj.getJSONArray("games").getJSONObject(1).getJSONArray("characterSet");

                        for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                            characterSetJSONObjArrayList.add(charactersSetJSONArray.getJSONObject(i));
                        }
                    }

                    //For Game 3
                    if (obj.getJSONArray("games").getJSONObject(2).getString("error") == "false") { //No error in the JSONArray "Game"
                        JSONArray charactersSetJSONArray = obj.getJSONArray("games").getJSONObject(2).getJSONArray("characterSet");

                        for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                            characterSetJSONObjArrayList.add(charactersSetJSONArray.getJSONObject(i));
                        }
                    }

                    //For Game 4
                    if (obj.getJSONArray("games").getJSONObject(3).getString("error") == "false") { //No error in the JSONArray "Game"
                        JSONArray charactersSetJSONArray = obj.getJSONArray("games").getJSONObject(3).getJSONArray("characterSet");

                        for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                            characterSetJSONObjArrayList.add(charactersSetJSONArray.getJSONObject(i));
                        }
                    }

                    /************ End : For adding the character set into json arraylist************/

                    /************ Start : Remove Duplicated json array************/
                    ArrayList<JSONObject> remarks = new ArrayList<JSONObject>();
                    //test: if there exists same character set, just remove it. (To optimize the download time by decreasing the unnecessary json array of character set)
                        for (int i = 0; i < characterSetJSONObjArrayList.size(); i++) {
                            for (int j = i + 1; j < characterSetJSONObjArrayList.size(); j++) {
                                // compare the json array, if there exist duplicated json array, remove it and set redo(Boolean) be true in order to redo
                                if (characterSetJSONObjArrayList.get(i).toString().equals(characterSetJSONObjArrayList.get(j).toString())) {
                                    remarks.add(characterSetJSONObjArrayList.get(j));
                                }
                            }
                        }
                    characterSetJSONObjArrayList.removeAll(remarks);

                    longInfo(characterSetJSONObjArrayList.toString() + "hihi");
                    /************ End : Remove Duplicated json array************/

                    //Set the character set to the characterArrayList
                    for (int k = 0; k < characterSetJSONObjArrayList.size(); k++) {
                        for (int i = 0; i < (characterSetJSONObjArrayList.get(k).length() - 1); i++) {
                            //Audio file name Array List
                            matcher = pattern.matcher(characterSetJSONObjArrayList.get(k).getJSONObject("" + i).getString("audio_url"));
                            if (matcher.find())
                                audioFileName.add(matcher.group(1));

                            //URL Array List to store URL of audio files
                            characterAudioArrayList.add(characterSetJSONObjArrayList.get(k).getJSONObject("" + i).getString("audio_url"));
                            Log.e("GCD", String.valueOf(characterAudioArrayList.get(i)));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                /************ Start : Downloading task ************/
                if (checkUpdate()) {
                    //Check if the folder exists, then create needed folder to contain audio files if necessary
                    if (newFolder.exists()) {
                        Log.e("Folder already exists: ", "true");
                    } else if (!newFolder.exists()) {
                        newFolder.mkdir();
                    }

                    //Instantiate the URL array
                    urls = new URL[characterAudioArrayList.size()];

                    for (int j = 0; j < characterAudioArrayList.size(); j++) {
                        try {
                            urls[j] = new URL(characterAudioArrayList.get(j));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }

                    //Instantiate the object of the Async task for downloading the audio files
                    dft = (DownloadFilesTask) new DownloadFilesTask().execute(urls);
                } else {
                    pb.setProgress(100);
                    progressIndicator.setText("Done");
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                /************ End : Downloading task ************/
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.w("GameLoadFiles-onFailure", "Status Code: " + String.valueOf(statusCode));
                Log.w("Game1LoadFiles", "Response String: " + responseString);
            }
        });
    }

    //If there is a need to update, return true; If no need to update, then return false
    private boolean checkUpdate() {
        Boolean audioFileExistsFlag = false;
        File audioFilePath = null;
        for (String matcher : audioFileName) {
            final File folder = new File("/sdcard/chinesegamecharactergame_audios");
            String folderPath = folder.getAbsolutePath() + "/";
            audioFilePath = new File(folderPath + matcher);
            Log.w("audioFilePath", audioFilePath.toString());

            Log.w("audioFilePath.exists()", audioFilePath.exists() + "");
            //Check if the audio file exists in the user's device
            if (audioFilePath.exists()) {
                audioFileExistsFlag = true;
            } else {
                audioFileExistsFlag = false;
            }
        }

        Log.d("audioFileExistsFlag: ", "" + audioFileExistsFlag);

        if (audioFileExistsFlag)
            return false;
        else
            return true;
    }

    private void hideStatusBar() {
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    private class DownloadFilesTask extends AsyncTask<URL, Integer, Long> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Long doInBackground(URL... urls) {
            long count = 0;
            int inputCount;
            int lengthOfFile = 0;
            long totalSize = 0;


            Log.d("URL SIZE IN DO", urls.length+"");
            //Calculate the totalSize for progress bar
            for (int a = 0; a < urls.length; a++) {
                totalSize += Downloader.downloadFile(urls[a]);
                Log.d("Totalsize : ", + totalSize + " index: "+a);
            }

            Log.w("Warning TotalSize ", totalSize + "");

            for (int i = 0; i < urls.length; i++) {
                Log.w("URL" + i, urls[i].toString());
                try {
                    URLConnection connection = urls[i].openConnection();
                    connection.connect();
                    // getting file length
                    count = count + connection.getContentLength();
                    Log.w("Warning Count ", "" + count);

                    // input stream to read file - with 19k buffer
                    InputStream input = new BufferedInputStream(urls[i].openStream(), 19192);
                    OutputStream output;
                    Boolean audioFileExistsFlag;
                    String folderPath = newFolder.getAbsolutePath() + "/";
                    final File audioFilePath = new File(folderPath + audioFileName.get(i));

                    //Check if the audio file exists in the user's device
                    if (audioFilePath.exists()) {
                        audioFileExistsFlag = true;
                    } else {
                        audioFileExistsFlag = false;
                    }

                    //If there is no same file in the device, then create the path by the audios' filenames
                    if (!audioFileExistsFlag) {
                        if (urls[i].toString().contains(".wav")) {
                            Log.w("contains wav", "contains wav");
                            output = new FileOutputStream(folderPath + audioFileName.get(i));
                        } else {
                            Log.w("contains mp3", "contains mp3");
                            output = new FileOutputStream(folderPath + audioFileName.get(i));
                        }

                        byte data[] = new byte[1024];

                        while ((inputCount = input.read(data)) != -1) {
                            output.write(data, 0, inputCount);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();
                    } else if (audioFileExistsFlag) {
                        Log.e("Audio File exists : ", "" + audioFilePath);
                    }

                    //Update the progress bar
                    publishProgress((int) (((double) count / totalSize) * 100));

                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                }
            }
            return totalSize;
        }

        protected void onProgressUpdate(Integer... progress) {
            pb.setProgress(progress[0]);
            //Update the progress bar by showing %
            progressIndicator.setText("Game Loading: " + String.valueOf(pb.getProgress() + "%"));
        }

        protected void onPostExecute(Long result) {
            //When finishing the download
            progressIndicator.setText("Done");

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
