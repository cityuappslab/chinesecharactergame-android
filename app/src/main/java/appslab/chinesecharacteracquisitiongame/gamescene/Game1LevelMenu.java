package appslab.chinesecharacteracquisitiongame.gamescene;

import android.app.ProgressDialog;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicInteger;

import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.dialogs.StageCompletedFragment;
import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameDataHelper;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameLevel;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameLevelShared;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;
import cz.msebera.android.httpclient.Header;

public class Game1LevelMenu extends AppCompatActivity implements StageCompletedFragment.OnItemClickedListener {

    private static final String TAG = Game1LevelMenu.class.getSimpleName();
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    static int stage;
    ProgressDialog progressDialog;
    Gson gson = new Gson();
    static GameLevelShared GLS = GameLevelShared.getInstance();
    int gameLevel;
    boolean shouldLoadOnresume = false;
    private static final int game_id = GameDataHelper.GAME_1_ID;
    private static final Class aClass = Game1.class;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_level_menu);
        shouldLoadOnresume = false;

        //Set the level data to the image buttons
        getLevelDataFromServer();
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

    }



    private void setupFragment() {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        if (mSectionsPagerAdapter == null)
            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        int length = tabLayout.getTabCount();
        for (int i = 0; i < length; i++) {
            tabLayout.getTabAt(i).setCustomView(mSectionsPagerAdapter.getTabView(i));
        }

        //Floating Button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setBackgroundTintList(getResources().getColorStateList(R.color.colorFloatingButton));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Game1LevelMenu.this, Games.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

    private void getLevelDataFromServer() {
        //Clear the GLS for pseudo real-time update the game level data in gameLevelMenu
        GLS.getGameLevels().clear();

        APIConnection.sharedInstance.getGameData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                JSONObject obj = response;

                try {
                    String error = String.valueOf(obj.getString("error"));
                    if (error.equals("true")) {  //Get Game Data unsuccessfully
                        Toast.makeText(getApplicationContext(), "Fail to get the Game Data. ", Toast.LENGTH_LONG).show();
                        finish();
                    } else if (error.equals("false")) {  //Get Game Data successfully
                        if (obj.getJSONArray("games").getJSONObject(0).getString("error").equals("false")) { //No error in the JSONArray "Game"
                            JSONArray levelsJSONArray = obj.getJSONArray("games").getJSONObject(0).getJSONArray("GameLevel");    //Get the JSONArray of different game levels

                            for (int a = 0; a < levelsJSONArray.length(); a++) {
                                JSONObject levelJSONObj = (JSONObject) levelsJSONArray.getJSONObject(a);

                                Log.d("levelJSONObj", "" + levelJSONObj);

                                //gson usage example: http://blog.tonycube.com/2012/03/gsonjavajson.html
                                GameLevel gl = gson.fromJson(String.valueOf(levelJSONObj), GameLevel.class);
                                GLS.addLevel(gl);
                            }

                            for (int b = 0; b < levelsJSONArray.length(); b++) {
                                Log.d("DISAPPEAR TIME", GLS.getGameLevels().get(b).getText_disappear_time() + "");
                            }
                            //Refresh the stage that the user can dive into = > Update the user level
                            getStageData();

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                //When getting the data, a dialog is shown
                progressDialog = ProgressDialog.show(Game1LevelMenu.this, "Loading...",
                        "Please wait..", true);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                long progressPercentage = (long) 100 * bytesWritten / totalSize;
                progressDialog.setProgress((int) progressPercentage);
            }

            @Override
            public void onFinish() {

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.w("Game1LvMenu-onFailure", String.valueOf(statusCode));
                Log.w("Game1LvMenu-onFailure", responseString);
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (shouldLoadOnresume) {
            Log.d("sunny", "loaded onResume");
            getLevelDataFromServer();
        }
        shouldLoadOnresume = true;
    }

    private void showStageCompletedDialog() {
        boolean shouldShowStageCompleted = SharedPrefsData.getShouldShowStageCompletedDialog(getApplicationContext());


        /**
         * starts the game directly if the dialog is set to never show
         */
        if (shouldShowStageCompleted ) {
            StageCompletedFragment stageCompletedFragment = StageCompletedFragment.newInstance();
            stageCompletedFragment.show(getFragmentManager(), "stageCompleted");
            (new SoundController()).playStageCompletedSound(this);
        }
    }

    public void getStageData() throws JSONException {
        APIConnection.sharedInstance.getStageData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    String error = String.valueOf(response.getString("error"));
                    if (error == "true") {  //Get Game Data unsuccessfully
                        Toast.makeText(getApplicationContext(), "Fail to get the Game Data. ", Toast.LENGTH_LONG).show();
                        finish();
                    } else if (error == "false") {  //Get Game Data successfully
                        Log.d("sunny", "stageData"+response.toString());
                        int stageNumber = Integer.parseInt(response.getString("stage"));

                        /**
                         * search the corresponding current level of the game
                         */

                        JSONArray currentLevel = response.getJSONArray("currentLevel");
                        for (int i = 0; i < currentLevel.length(); i++) {
                            JSONObject jsonObject = response.getJSONArray("currentLevel").getJSONObject(i);
                            int game_id_json = Integer.parseInt(jsonObject.getString("game_id"));
                            if (game_id_json == game_id) {

                                int level = Integer.parseInt(jsonObject.getString("level"));
                                gameLevel = level;
                                Log.d(TAG, "game level after looping: " + level);
                                if (!GeneralUtil.DEBUG_TEST_LEVEL)
                                    SharedPrefsData.setGameLevel(getApplicationContext(), game_id, level);
                            }
                        }
//                        int gameLevel = Integer.parseInt(response.getJSONArray("currentLevel").getJSONObject(0).getString("level"));
                        stage = stageNumber;
                        if (!GeneralUtil.DEBUG_TEST_STAGE)
                            SharedPrefsData.setStageNumber(getApplicationContext(), stageNumber);
                        Log.d(this.getClass().getSimpleName() + "Stage Data: ", response.toString());
                        Log.d(this.getClass().getSimpleName() + "Stage Number: ", String.valueOf(stageNumber));
                        Log.d(this.getClass().getSimpleName() + "gameLevel: ", String.valueOf(gameLevel));
                        if (mSectionsPagerAdapter != null) {
                            mSectionsPagerAdapter.notifyDataSetChanged();
                            GeneralUtil.loggingHelper(getApplicationContext(), "notified dataset changed", "");
                        }


                    }
                    //TODO add checking if need not refresh
                    setupFragment();

                    showStageCompletedDialog();

                } catch (JSONException e) {
                    Log.d("sunny", "Gamemenu: "+e);
                    Log.d("sunny", "Gamemenu: "+e.getMessage());
                    if (e.getMessage().equals("No value for currentLevel")) {
                        setupFragment();
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                    e.printStackTrace();

                }
            }

            @Override
            public void onStart() {
                //When getting the data, a dialog is shown
                progressDialog = ProgressDialog.show(Game1LevelMenu.this, "Loading...",
                        "Please wait..", true);
            }

            @Override
            public void onFinish() {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.w(this.getClass().getSimpleName() + "-onFailure", String.valueOf(statusCode));
                Log.w(this.getClass().getSimpleName() + "-onFailure", responseString);
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onItemClicked() {

    }

    //Stage 1 Fragment
    public static class StageOneFragment extends Fragment {
        private static final String STAGE = "section_number";
        ImageButton IB[] = new ImageButton[20];

        public StageOneFragment() {

        }

        public static StageOneFragment newInstance(int sectionNumber) {
            StageOneFragment fragment = new StageOneFragment();
            Bundle args = new Bundle();
            args.putInt(STAGE, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_game1_level_menu1, container, false);

            return rootView;
        }

        @Override
        public void onDestroyView() {
            // TODO Auto-generated method stub
            super.onDestroyView();
            try {
                Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
                childFragmentManager.setAccessible(true);
                childFragmentManager.set(this, null);


            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        //stage 1 fragment
        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            //get the checked game level
            int currentGameLevel_1 = GameDataHelper.getGameLevel(getContext(), game_id, 1);
            GeneralUtil.loggingHelper(getActivity(), "getGameLevel: ", String.valueOf(SharedPrefsData.getGameLevel(getContext(), game_id)));

            for (int i = 1; i <= currentGameLevel_1; i++) {
                AtomicInteger playingLevel = new AtomicInteger(i);

                //Find image button
                String buttonID = "button" + i;
                int buttonResID = getResources().getIdentifier(buttonID, "id",
                        getActivity().getPackageName());
                IB[i - 1] = (ImageButton) getView().findViewById(buttonResID);

                //Set the images' sources for the image buttons
                String imageName = "first_game_" + i;   //From first_game_1 to first_game_20
                int imageResName = getResources().getIdentifier(imageName, "drawable",
                        getActivity().getPackageName());
                //Set image for the button
//                IB[i - 1].setImageDrawable(getResources().getDrawable(imageResName));
//                IB[i - 1].setImageResource(imageResName);
                Glide.with(getActivity())
                        .load("")
                        .placeholder(imageResName)
                        .into(IB[i-1]);
                IB[i - 1].setAlpha((float) 1);
                //Whenever the button is clicked, the "playing level" will be sent to the Game1
                IB[i - 1].setId(playingLevel.get());

                IB[i - 1].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getView().getContext(), aClass);
                        intent.putExtra("PLAYING_LEVEL", (view.getId()));
                        Log.d(TAG, "playing level: " + view.getId());
                        intent.putExtra("STAGE", 1);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        for (int a = 0;a<20;a++) {
//                            ((BitmapDrawable) IB[a].getDrawable()).getBitmap().recycle();
//                        }
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
                IB[i - 1].setClickable(true);
            }

            for (int i = 20; i > currentGameLevel_1; i--) {
                AtomicInteger playingLevel = new AtomicInteger(i);
//                final double textDisappearTime = GLS.getGameLevels().get(i).getText_disappear_time();
//                Log.d("TEXT_DISAPPEAR_TIME", textDisappearTime+"");

                //Find image button
                String buttonID = "button" + i;
                int buttonResID = getResources().getIdentifier(buttonID, "id",
                        getActivity().getPackageName());
                IB[i - 1] = (ImageButton) getView().findViewById(buttonResID);

                //Set the images' sources for the image buttons
                String imageName = "first_game_lock";   //From first_game_1 to first_game_20
                int imageResName = getResources().getIdentifier(imageName, "drawable",
                        getActivity().getPackageName());
                //Set image for the button
//                IB[i - 1].setImageDrawable(getResources().getDrawable(imageResName));
//                IB[i - 1].setImageResource(imageResName);
                Glide.with(getActivity())
                        .load("")
                        .placeholder(imageResName)
                        .into(IB[i-1]);
                IB[i - 1].setAlpha((float) 1);
                //Whenever the button is clicked, the "playing level" will be sent to the Game1
                IB[i - 1].setId(playingLevel.get());


                IB[i - 1].setClickable(false);
            }
        }
    }

    //Stage 2 Fragment
    public static class StageTwoFragment extends Fragment {
        private static final String STAGE = "section_number";
        ImageButton IB[] = new ImageButton[20];

        @Override
        public void onDestroyView() {
            // TODO Auto-generated method stub
            super.onDestroyView();
            try {
                Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
                childFragmentManager.setAccessible(true);
                childFragmentManager.set(this, null);


            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        public StageTwoFragment() {

        }

        public static StageTwoFragment newInstance(int sectionNumber) {
            StageTwoFragment fragment = new StageTwoFragment();
            Bundle args = new Bundle();
            args.putInt(STAGE, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_game1_level_menu2, container, false);

            return rootView;
        }

        //stage 2 fragment
        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            int currentGameLevel_1 = GameDataHelper.getGameLevel(getContext(), game_id, 2);
            GeneralUtil.loggingHelper(getActivity(), "getGameLevel: ", String.valueOf(SharedPrefsData.getGameLevel(getContext(), game_id)));

            for (int i = 1; i <= currentGameLevel_1; i++) {
                AtomicInteger playingLevel = new AtomicInteger(i);

                //Find image button
                String buttonID = "button" + i;
                int buttonResID = getResources().getIdentifier(buttonID, "id",
                        getActivity().getPackageName());
                IB[i - 1] = (ImageButton) getView().findViewById(buttonResID);

                //Set the images' sources for the image buttons
                String imageName = "first_game_" + (20 + i);   //From first_game_1 to first_game_20
                int imageResName = getResources().getIdentifier(imageName, "drawable",
                        getActivity().getPackageName());
                //Set image for the button
//                IB[i - 1].setImageResource(imageResName);
                Glide.with(getActivity())
                        .load("")
                        .placeholder(imageResName)
                        .into(IB[i-1]);
                IB[i - 1].setAlpha((float) 1);
                //Whenever the button is clicked, the "playing level" will be sent to the Game1
                IB[i - 1].setId(playingLevel.get());

                IB[i - 1].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getView().getContext(), aClass);
                        intent.putExtra("PLAYING_LEVEL", (view.getId())+GameDataHelper.STAGE_2_LEVEL_ADDITION);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        Log.d(TAG, "playing level: " + view.getId()+GameDataHelper.STAGE_2_LEVEL_ADDITION);
                        intent.putExtra("STAGE", 2);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
                IB[i - 1].setClickable(true);
            }


            //use to lock all the levels before 4 games have unlocked the first 20 levels
            for (int i = 20; i > currentGameLevel_1; i--) {
                AtomicInteger playingLevel = new AtomicInteger(i);
//                final double textDisappearTime = GLS.getGameLevels().get(i).getText_disappear_time();
//                Log.d("TEXT_DISAPPEAR_TIME", textDisappearTime+"");

                //Find image button
                String buttonID = "button" + i;
                int buttonResID = getResources().getIdentifier(buttonID, "id",
                        getActivity().getPackageName());
                IB[i - 1] = (ImageButton) getView().findViewById(buttonResID);

                //Set the images' sources for the image buttons
                String imageName = "first_game_lock";   //From first_game_1 to first_game_20
                int imageResName = getResources().getIdentifier(imageName, "drawable",
                        getActivity().getPackageName());
                //Set image for the button
//                IB[i - 1].setImageResource(imageResName);
                Glide.with(getActivity())
                        .load("")
                        .placeholder(imageResName)
                        .into(IB[i-1]);

                IB[i - 1].setAlpha((float) 1);
                //Whenever the button is clicked, the "playing level" will be sent to the Game1
                IB[i - 1].setId(playingLevel.get());

                IB[i - 1].setClickable(false);
            }
//
//            int currentGameLevel_2 = GameDataHelper.getGameLevel(getContext(),11,2);
//            GeneralUtil.loggingHelper(getActivity(),"getGameLevel: ", String.valueOf(SharedPrefsData.getGameLevel(getContext(),11)));
//
//            for (int i = currentGameLevel_2; i > 0; i--) {
//                AtomicInteger playingLevel = new AtomicInteger(i);
//                stage = 1;
//
//                //Find image button
//                String buttonID = "button" + i;
//                int buttonResID = getResources().getIdentifier(buttonID, "id",
//                        getActivity().getPackageName());
//                IB[i-1] = (ImageButton) getView().findViewById(buttonResID);
//
//                //Set the images' sources for the image buttons
////                String imageName = "first_game_" + (20 + i);   //From first_game_21 to first_game_40
//                String imageName = "first_game_lock";
//                int imageResName = getResources().getIdentifier(imageName, "drawable",
//                        getActivity().getPackageName());
//                IB[i-1].setImageDrawable(getResources().getDrawable(imageResName));
//                //Whenever the button is clicked, the "playing level" will be sent to the Game1
//                IB[i-1].setId(playingLevel.get());
//
//                IB[i-1].setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(getView().getContext(), Game1.class);
//                        intent.putExtra("PLAYING_LEVEL", (""+view.getId()));
//                        intent.putExtra("STAGE", stage);
//                        startActivity(intent);
//                    }
//                });
//                IB[i-1].setClickable(false);
//            }
//
//            if(getArguments().getInt("section_number") == 2 || getArguments().getInt("section_number") == 3) {
//                for (int i = 1; i <= currentGameLevel_2; i++) {
//                    IB[i-1].setAlpha((float)1);
//                    IB[i-1].setClickable(true);
//                }
//            } else if (getArguments().getInt("section_number") == 1) {
//                for (int i = 1; i <= currentGameLevel_2; i++) {
//                    IB[i-1].setAlpha((float)0.2);
//                    IB[i-1].setClickable(true);
//                }
//            }
        }

    }

    //Stage 3 Fragment
    public static class StageThreeFragment extends Fragment {
        private static final String STAGE = "section_number";
        ImageButton IB[] = new ImageButton[20];

        @Override
        public void onDestroyView() {
            // TODO Auto-generated method stub
            super.onDestroyView();
            try {
                Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
                childFragmentManager.setAccessible(true);
                childFragmentManager.set(this, null);


            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        public StageThreeFragment() {

        }

        public static StageThreeFragment newInstance(int sectionNumber) {
            StageThreeFragment fragment = new StageThreeFragment();
            Bundle args = new Bundle();
            args.putInt(STAGE, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_game1_level_menu3, container, false);

            return rootView;
        }

        public void onTrimMemory(int level) {

            // Determine which lifecycle or system event was raised.
            switch (level) {

                case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                    break;

                case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
                case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
                case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                    break;

                case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
                case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
                case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                    break;

                default:
                    break;
            }
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            int currentGameLevel_1 = GameDataHelper.getGameLevel(getContext(), game_id, 3);
            GeneralUtil.loggingHelper(getActivity(), "getGameLevel: ", String.valueOf(SharedPrefsData.getGameLevel(getContext(), game_id)));


            for (int i = 1; i <= currentGameLevel_1; i++) {
                AtomicInteger playingLevel = new AtomicInteger(i);

                //Find image button
                String buttonID = "button" + i;
                int buttonResID = getResources().getIdentifier(buttonID, "id",
                        getActivity().getPackageName());
                IB[i - 1] = (ImageButton) getView().findViewById(buttonResID);

                //Set the images' sources for the image buttons
                String imageName = "first_game_" + (40 + i);   //From first_game_1 to first_game_20
                int imageResName = getResources().getIdentifier(imageName, "drawable",
                        getActivity().getPackageName());
                //Set image for the button
//                IB[i - 1].setImageResource(imageResName);
                Glide.with(getActivity())
                        .load("")
                        .placeholder(imageResName)
                        .into(IB[i-1]);
                IB[i - 1].setAlpha((float) 1);
                //Whenever the button is clicked, the "playing level" will be sent to the Game1
                IB[i - 1].setId(playingLevel.get());

                IB[i - 1].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getView().getContext(), aClass);
                        intent.putExtra("PLAYING_LEVEL", (view.getId())+GameDataHelper.STAGE_3_LEVEL_ADDITION);
                        Log.d(TAG, "playing level: " + view.getId()+GameDataHelper.STAGE_3_LEVEL_ADDITION);
                        intent.putExtra("STAGE", 3);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
                IB[i - 1].setClickable(true);
            }


            //use to lock all the levels before 4 games have unlocked the first 40 levels
            for (int i = 20; i > currentGameLevel_1; i--) {
                AtomicInteger playingLevel = new AtomicInteger(i);
//                final double textDisappearTime = GLS.getGameLevels().get(i).getText_disappear_time();
//                Log.d("TEXT_DISAPPEAR_TIME", textDisappearTime+"");

                //Find image button
                String buttonID = "button" + i;
                int buttonResID = getResources().getIdentifier(buttonID, "id",
                        getActivity().getPackageName());
                IB[i - 1] = (ImageButton) getView().findViewById(buttonResID);

                //Set the images' sources for the image buttons
                String imageName = "first_game_lock";   //From first_game_1 to first_game_20
                int imageResName = getResources().getIdentifier(imageName, "drawable",
                        getActivity().getPackageName());
                //Set image for the button
//                IB[i - 1].setImageResource(imageResName);

                Glide.with(getActivity())
                        .load("")
                        .placeholder(imageResName)
                        .into(IB[i-1]);
                IB[i - 1].setAlpha((float) 1);
                //Whenever the button is clicked, the "playing level" will be sent to the Game1
                IB[i - 1].setId(playingLevel.get());

                IB[i - 1].setClickable(false);
            }

//            int currentGameLevel_3 = GameDataHelper.getGameLevel(getContext(),11,3);
//            GeneralUtil.loggingHelper(getActivity(),"getGameLevel: ", String.valueOf(SharedPrefsData.getGameLevel(getContext(),11)));
//            for (int i = currentGameLevel_3; i > 0; i--) {
//                AtomicInteger playingLevel = new AtomicInteger(i);
//                stage = 1;
//
//                //Find image button
//                String buttonID = "button" + i;
//                int buttonResID = getResources().getIdentifier(buttonID, "id",
//                        getActivity().getPackageName());
//                IB[i-1] = (ImageButton) getView().findViewById(buttonResID);
//
//                //Set the images' sources for the image buttons
////                String imageName = "first_game_" + (40 + i);   //From first_game_41 to first_game_60
//                String imageName = "first_game_lock";
//                int imageResName = getResources().getIdentifier(imageName, "drawable",
//                        getActivity().getPackageName());
//                IB[i-1].setImageDrawable(getResources().getDrawable(imageResName));
//                //Whenever the button is clicked, the "playing level" will be sent to the Game1
//                IB[i-1].setId(playingLevel.get());
//
//                IB[i-1].setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(getView().getContext(), Game1.class);
//                        intent.putExtra("PLAYING_LEVEL", (""+view.getId()));
//                        intent.putExtra("STAGE", stage);
//                        startActivity(intent);
//                    }
//                });
//                IB[i-1].setClickable(false);
//            }
//            IB[5].setAlpha((float)1);
//
//            if(getArguments().getInt("section_number") == 2 || getArguments().getInt("section_number") == 3) {
//                for (int i = 1; i <= currentGameLevel_3; i++) {
//                    IB[i-1].setAlpha((float)1);
//                    IB[i-1].setClickable(true);
//                }
//            } else if (getArguments().getInt("section_number") == 1){
//                for (int i = 1; i <= currentGameLevel_3; i++) {
//                    IB[i-1].setAlpha((float)0.2);
//                    IB[i-1].setClickable(true);
//                }
//            }
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return StageOneFragment.newInstance(1);
                case 1:
                    return StageTwoFragment.newInstance(2);
                case 2:
                    return StageThreeFragment.newInstance(2);
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "STAGE 1";
                case 1:
                    return "STAGE 2";
                case 2:
                    return "STAGE 3";
            }
            return null;
        }

        public View getTabView(int tabPosition) {
            switch (tabPosition) {
                case 0:
                    return getLayoutInflater().inflate(R.layout.game1_tab_custom_view, null);
                case 1:
                    return getLayoutInflater().inflate(R.layout.game1_tab_custom_view_2, null);
                case 2:
                    return getLayoutInflater().inflate(R.layout.game1_tab_custom_view_3, null);
            }
            return null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);
        unbindDrawables(coordinatorLayout);
    }

    public void unbindDrawables(View view) {//pass your parent view here
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
                if (view instanceof ImageButton)
                    ((ImageButton)view).setImageDrawable(null);
                Log.d(TAG, "clear image drawable");
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(Game1LevelMenu.this, Games.class);
        startActivity(intent);
        finish();

    }
}