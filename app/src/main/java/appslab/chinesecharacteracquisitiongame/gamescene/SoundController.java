package appslab.chinesecharacteracquisitiongame.gamescene;

import android.content.Context;
import android.media.MediaPlayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import appslab.chinesecharacteracquisitiongame.R;

/**
 * Created by Johnny on 7/19/16.
 */
public class SoundController {
    private MediaPlayer mediaPlayer;

    public void playStageCompletedSound( Context ctx){
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(ctx, R.raw.stage_completed_sound);
        }
        if ((mediaPlayer != null) && (!mediaPlayer.isPlaying())) {
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(ctx, R.raw.stage_completed_sound);
            mediaPlayer.start();
        }
    }

    public void playCorrectSound(Context ctx) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(ctx, R.raw.correct);
        }

        if ((mediaPlayer != null) && (!mediaPlayer.isPlaying())) {
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(ctx, R.raw.correct);
            mediaPlayer.start();
        }
    }

    public void playWrongSound(Context ctx) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(ctx, R.raw.wrong);
        }

        if ((mediaPlayer != null) &&  (!mediaPlayer.isPlaying())) {
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(ctx, R.raw.wrong);
            mediaPlayer.start();
        }
    }

    public void playWinSoundTrack(Context ctx) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(ctx, R.raw.win_dialog_mp3);
        }

        if ((mediaPlayer != null) &&  (!mediaPlayer.isPlaying())) {
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(ctx, R.raw.win_dialog_mp3);
            mediaPlayer.start();
        }
    }

    public void playLoseSoundTrack(Context ctx) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(ctx, R.raw.fail_dialog);
        }

        if ((mediaPlayer != null) &&  (!mediaPlayer.isPlaying())) {
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(ctx, R.raw.fail_dialog);
            mediaPlayer.start();
        }
    }

    public void playButtonSoundTrack(Context ctx) {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(ctx, R.raw.btnsound);
        }

        if ((mediaPlayer != null) &&  (!mediaPlayer.isPlaying())) {
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(ctx, R.raw.btnsound);
            mediaPlayer.start();
        }
    }

    public void playWordSound(String audioPath, Context ctx) throws IOException {
        File filePath = new File("/sdcard/chinesegamecharactergame_audios/" + audioPath);
        FileInputStream is = new FileInputStream(filePath);
        if (mediaPlayer == null) {
            mediaPlayer = new  MediaPlayer();
            mediaPlayer.setDataSource(is.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();
        }

        if ((mediaPlayer != null) &&  (!mediaPlayer.isPlaying())) {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(is.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();
        }
        is.close();
    }
}
