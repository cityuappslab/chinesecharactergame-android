package appslab.chinesecharacteracquisitiongame.gamescene;

import android.animation.ObjectAnimator;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.ComponentCallbacks2;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.dialogs.IntroductionFragmentGame5Bird;
import appslab.chinesecharacteracquisitiongame.dialogs.LevelCompletedFragment;
import appslab.chinesecharacteracquisitiongame.dialogs.LevelFailedFragment;
import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;
import appslab.chinesecharacteracquisitiongame.gamesetting.Character;
import appslab.chinesecharacteracquisitiongame.gamesetting.Game5BirdTimeStamp;
import appslab.chinesecharacteracquisitiongame.gamesetting.Game5Controller;
import appslab.chinesecharacteracquisitiongame.gamesetting.Game5Timer;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameCoreData;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameDataHelper;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameLevelShared;
import appslab.chinesecharacteracquisitiongame.gamesetting.GameTrial;
import appslab.chinesecharacteracquisitiongame.networkconnection.MatrixPosition;
import cz.msebera.android.httpclient.Header;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;
import appslab.chinesecharacteracquisitiongame.networkconnection.DeviceConnection;
import cz.msebera.android.httpclient.entity.StringEntity;

public class Game5_Bird extends Activity implements View.OnClickListener, View.OnTouchListener, Game5Controller,IntroductionFragmentGame5Bird.OnItemClickedListener, LevelFailedFragment.FailFragmentOnItemClickedListener, LevelCompletedFragment.LevelCompletedFragmentOnItemClickedListener {

    String audio;
    Character answer;
    Character[][] characters;
    LinearLayout linear;
    ImageButton sound, ibGame5BirdPause;
    ProgressBar progressBar;
    Game5Timer game5Timer;
    GameCoreData gcd;
    ObjectAnimator animation;
    ArrayList<String> characterAudioSet = new ArrayList<String>();
    ArrayList<String> characterArrayList = new ArrayList<String>();
    ArrayList<JSONObject> characterSetJSONObjArrayList = new ArrayList<JSONObject>();
    //A regular expression to determine the audio file name (Include the extension name) in a URL
    Pattern pattern = Pattern.compile("http:\\/\\/.*\\/character\\/audio\\/(.*)");
    Matcher matcher;
    SoundController sc;
    double accuracy = 0.0;
    private MediaPlayer mediaPlayer;
    TextView editUserDisplayName;
    ProgressDialog progressDialog;
    TextView instruction;
    int playingLevel;
    LevelCompletedFragment lcf;
    LevelFailedFragment lff;
    IntroductionFragmentGame5Bird introductionFragmentGame5Bird;
    FragmentManager fm = getFragmentManager();
    ImageView levelUpIcon;
    ArrayList<Integer> list = new ArrayList<Integer>();
    static GameLevelShared GLS = GameLevelShared.getInstance();
    JSONObject resultUpload = null;
    MatrixPosition matrixPosition;
    Game5BirdTimeStamp game5birdTimeStamp = new Game5BirdTimeStamp();
    boolean isOnClickForWholeClass = false;
    int DELAY_TO_NEXT = 2000;
    boolean isProcessGameEnd = true;
    //for fixing the resume problem when resumed button is clicked after wrong word is chosen
    boolean isWrongWordSelected = true;

    private static final int game_id = GameDataHelper.GAME_3_ID;

    int stage;

    int row = 4, col = 5;

    String ans = "A";
    int ansrow = 0, anscol = 0;

    @Override
    public void onBackPressed() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (animation != null) {
                    animation.cancel();
                }
            }
        });
        gcd.setGameStatus("pause");
        ibGame5BirdPause.performClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game5_bird_main);

        //For recording the start time of the game and submit to server afterward
        game5birdTimeStamp.createStartingTimeStamp();
        Log.d("Game 1 TimeStamp : ", game5birdTimeStamp.getStartingTimeStamp() + "");

        //Get the small data from the previous activity
        getExtras();

        DeviceConnection dc = new DeviceConnection();
        if (dc.checkNetworkAvailability(Game5_Bird.this)) {   //Check if the device connected to the Internet
            getCoreData();
        } else {
            finish();
        }

    }

    public void createCharacter() {
        isWrongWordSelected = false;
        isProcessGameEnd = false;
        //The message to guide the user how to play
        instruction.setVisibility(View.VISIBLE);
        characters = new Character[row][col];

        //Remove all the words
        linear = (LinearLayout) findViewById(R.id.linearClick);
        linear.removeAllViews();
        linear.setGravity(Gravity.CENTER);
        linear.setPadding(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics()),-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics()),0,0);

        //Clear the history of generated random number
        list.clear();
        matrixPosition = new MatrixPosition();
        matrixPosition.clearHashMap();

        //Prepare a list in order to generate non-repeated number
        list = new ArrayList<Integer>();
        for (int a = 0; a < characterArrayList.size(); a++) {
            list.add(a);
        }
        //set
        for (int i = 0; i < characters.length; i++) {
            LinearLayout l = new LinearLayout(this);
            l.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            l.setGravity(Gravity.CENTER_HORIZONTAL);
            l.setLayoutParams(params);
            for (int k = 0; k < characters[i].length; k++) {
                int numThisTime = 0;

                //Prepare the random number for creation of the words
                while (list.size() != 0) {
                    Collections.shuffle(list);
                    numThisTime = list.get(0);
                    list.remove(0);
                    break;
                }

//                Random rand = new Random();
//                int n = rand.nextInt(5);
                Character c = new Character();
//                c.setOneCharacter(characterArrayList.get(n));
//                c.setAudio_url(characterAudioSet.get(n));
                c.setOneCharacter(characterArrayList.get(numThisTime));
                c.setAudio_url(characterAudioSet.get(numThisTime));
                TextView ib = new TextView(this);
                ib.setBackground(ContextCompat.getDrawable(this, R.drawable.oval));

                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                int HInDp = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 40, getResources()
                                .getDisplayMetrics());
                int WInDp = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 40, getResources()
                                .getDisplayMetrics());
                p.height=HInDp;
                p.width=WInDp;
                p.setMargins(15,15,15,15);
                ib.setLayoutParams(p);
                ib.requestLayout();
                ib.setOnClickListener(this);
                ib.setOnTouchListener(this);
                ib.setText(characterArrayList.get(numThisTime));
                ib.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT, 3, getResources().getDisplayMetrics()));
                ib.setTextColor(Color.WHITE);
                ib.setGravity(Gravity.CENTER);
                c.setIb(ib);
                characters[i][k] = c;
                //insert the position of the words into the map(matrixPosition)
                matrixPosition.setMatrixPosition(i, k, c);
                l.addView(ib);
            }
            linear.addView(l);
        }

        //Hide the words before the user start the game by pressing the sound(Animal) button
        linear.setVisibility(View.INVISIBLE);
    }

    @Override
    public void removeTargetWord() {
        //Do nothing
    }

    public void getCoreData() {
        gcd = new GameCoreData();

        APIConnection.sharedInstance.getGameData(null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray

                try {
                    String error = String.valueOf(response.getString("error"));
                    switch (error) {
                        //Fail to get game data
                        case "true":
                            Toast.makeText(getApplicationContext(), "Fail to get the Game Data. ", Toast.LENGTH_LONG).show();
                            finish();
                            break;
                        //Get Game Data successfully
                        case "false":
                            if (response.getJSONArray("games").getJSONObject(2).getString("error").equals("false")) { //No error in the JSONArray "Game"
                                JSONArray levels = response.getJSONArray("games").getJSONObject(2).getJSONArray("GameLevel"); //Get the JSONArray of different game levels
                                JSONArray charactersSetJSONArray = response.getJSONArray("games").getJSONObject(2).getJSONArray("characterSet");

                                for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                                    characterSetJSONObjArrayList.add(charactersSetJSONArray.getJSONObject(i));
                                }

                                //This snippet is for extracting the relevant character set (Create an index for characterSet)
                                int characterSetId = GLS.getGameLevels().get(playingLevel).getCharacterSet_id();
                                int characterSetIndex = 0;
                                for (int i = 0; i < charactersSetJSONArray.length(); i++) {
                                    if (characterSetId == Integer.parseInt(characterSetJSONObjArrayList.get(i).getString("characterSet_id"))) {
                                        characterSetIndex = i;
                                    }
                                }

                                //Set Levels
//                                for (int i = 0; i < levels.length(); i++) {
//                                    GameTrial gt = new GameTrial();
//                                    gt.setDuration(Double.valueOf(levels.getJSONObject(i).getString("duration")) * 1000);
//                                    gcd.addTrial(gt);
//                                }


                                //Set the character set 1 to the characterArrayList
                                for (int i = 0; i < (characterSetJSONObjArrayList.get(characterSetIndex).length() - 1); i++) {
                                    characterArrayList.add(characterSetJSONObjArrayList.get(characterSetIndex).getJSONObject("" + i).getString("oneCharacter"));
                                    Log.d("Game5 Bird:", characterSetJSONObjArrayList.get(characterSetIndex).getJSONObject("" + i).getString("oneCharacter"));
                                    characterAudioSet.add(characterSetJSONObjArrayList.get(characterSetIndex).getJSONObject("" + i).getString("audio_url"));
                                }

                                initializeUI();
                                prepareGameTrials();

                                row = levels.getJSONObject(playingLevel-1).getInt("numOfRow");
                                col = levels.getJSONObject(playingLevel-1).getInt("numOfCol");

                                setUserName();
                                //TODO set lvUp icon
                                setLVUPIcon();
                                createCharacter();
                                setAnswer();
                                showIntroductionDialog();
//                                startGame();
                            }
                            break;
                        default:
                            Toast.makeText(getApplicationContext(), "Login: UNKNOWN", Toast.LENGTH_LONG).show();
                            break;
                    }
//                    Toast.makeText(getApplicationContext(), "Game1-onSuccess", Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Fail to upload record", Toast.LENGTH_LONG).show();
                Log.d("Game5_Bird-onFailure","onFailure without JSONARRAY");
                Log.w("Game5_Bird-onFailure", String.valueOf(statusCode));
                Log.w("Game5_Bird-onFailure", responseString);
                progressDialog.dismiss();
            }

            @Override
            public void onStart() {
                //When getting the data, a dialog is shown
                progressDialog = ProgressDialog.show(Game5_Bird.this, "Loading...",
                        "Please wait..", true);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                //Update the progress bar
                long progressPercentage = (long) 100 * bytesWritten / totalSize;
                progressDialog.setProgress((int) progressPercentage);
            }

            @Override
            public void onFinish() {
                progressDialog.dismiss();
            }
        });
    }

    private void getExtras() {
        Bundle extras = getIntent().getExtras();
        playingLevel = extras.getInt("PLAYING_LEVEL");
        stage = extras.getInt("STAGE");
    }

    private void prepareGameTrials() {
        gcd = new GameCoreData();

        //20 Trials per level (Set 20 trials)
        for (int i = 0; i < 20; i++) {
            GameTrial gt = new GameTrial();
            Log.d("***", String.valueOf(((GameLevelShared.GLArrayList.get(playingLevel-1).getDuration())*1000)));
            Log.d("***", String.valueOf(((GameLevelShared.GLArrayList.get(playingLevel-1).getGamelevel_id()))));
            gt.setDuration(((GameLevelShared.GLArrayList.get(playingLevel-1).getDuration())*1000)); //Used the duration time from server
            gcd.addTrial(gt);
        }
    }

    private void setUserName() {
        editUserDisplayName = (TextView) findViewById(R.id.editUserDisplayName);
        SharedPreferences settings = getSharedPreferences("PREF", 0);
        String username = settings.getString("USER_NAME", "none");
        editUserDisplayName.setText(username);
    }

    private void setLVUPIcon() {
        levelUpIcon = (ImageView) findViewById(R.id.levelUpIcon_game_menu);
        int stage_number = SharedPrefsData.getStageNumber(getApplicationContext());
        switch (stage_number) {
            case 0://egg
                levelUpIcon.setImageResource(R.drawable.lvup_animal1);
                break;
            case 1://cracked egg
                levelUpIcon.setImageResource(R.drawable.lvup_animal2);
                break;
            case 2://little chick
                levelUpIcon.setImageResource(R.drawable.lvup_animal3);
                break;
            case 3://mother chick
                levelUpIcon.setImageResource(R.drawable.lvup_animal4);
                break;

        }
    }

    public void setAnswer() {
        Random rand = new Random();
        int n = rand.nextInt(characters.length);
        int n2 = rand.nextInt(characters[0].length);
        audio = characters[n][n2].getAudio_url();
        answer = characters[n][n2];
        //set the string of the target word
        ans = answer.getOneCharacter();
        ansrow = n;
        anscol = n2;
        Log.d("sunny", "Game5_Bat , getting the target word row col: " + "( " + (ansrow + 1) + "," + (anscol + 1) + " )" + "ans: " + ans);
    }


    public void initializeUI() {
        instruction = (TextView) findViewById(R.id.instruction);
        progressBar = (ProgressBar) findViewById(R.id.probar5bird);
        sc = new SoundController();

        sound = (ImageButton) findViewById(R.id.sound);
        sound.setOnTouchListener(this);
        sound.setOnClickListener(this);

        ibGame5BirdPause = (ImageButton) findViewById(R.id.ibGame5BirdPause);
        ibGame5BirdPause.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return TouchEvent.onTouch(v, event);
            }
        });
        ibGame5BirdPause.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (game5Timer != null) {
                    game5Timer.stop();
                    if (gcd.getGameStatus().equals("start")) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                animation.cancel();
                            }

                        });
                        gcd.setGameStatus("pause");
                    }
                }
                final Dialog dialog = new Dialog(Game5_Bird.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_pause);
                dialog.setCancelable(false);
                //there are a lot of settings, for dialog, check them all out!

                //set up image view
                ImageButton ibbacktomenu = (ImageButton) dialog.findViewById(R.id.ibbacktomenu);
                ibbacktomenu.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return TouchEvent.onTouch(v, event);
                    }
                });
                ibbacktomenu.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                        finish();
                    }
                });

                ImageButton ibresume = (ImageButton) dialog.findViewById(R.id.ibresume);
                ibresume.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return TouchEvent.onTouch(v, event);
                    }
                });
                ibresume.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                        //there are 3 cases to handle
                        if (gcd.getGameStatus().equals("pause") && !isProcessGameEnd && !isWrongWordSelected) {
                            if (game5Timer != null) {
                                resumeGame();
                            } else {
                                //No timer means the game not start yet, so set game status to stop, otherwise, sound.onClickListener will have problem
                                gcd.setGameStatus("stop");
                            }
                        }else if(isProcessGameEnd){
                            gcd.setGameStatus("stop");
                        }


                    }
                });
                dialog.cancel();
                dialog.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        int game_id = GameDataHelper.GAME_3_ID;
        Log.d("sunny", "Before: " + String.valueOf(SharedPrefsData.getGameLevel(getApplicationContext(), game_id)));
        if (GeneralUtil.DEBUG_TEST_LEVEL) {
            if (GeneralUtil.DEBUG_TEST_LEVEL) {
                int gameLevel = SharedPrefsData.getGameLevel(getApplicationContext(), game_id);
                SharedPrefsData.setGameLevel_debug(getApplicationContext(),
                        game_id,
                        gameLevel);
            }
            Log.d("sunny", "After: " + String.valueOf(SharedPrefsData.getGameLevel(getApplicationContext(), game_id)));
        }
        if (GeneralUtil.DEBUG_TEST_STAGE) {
            int stage_number = SharedPrefsData.getStageNumber(getApplicationContext());
            SharedPrefsData.setStageNumber_debug(getApplicationContext(), stage_number + 1);
        }
        unbindDrawables(linear);

    }

    private void showLevelFailedDialog(int tempAccuracy) {
        if (game5Timer != null) {
            game5Timer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        lff = LevelFailedFragment.newInstance(tempAccuracy);
        lff.setActivity(Game5_Bird.this);
//        accuracyTV = (TextView) lff.getDialog().findViewById(R.id.accuracy);
//        accuracyTV.setText(String.valueOf(accuracy));
        lff.show(fm, "Johnny");
        sc.playLoseSoundTrack(this);
    }

    private void showLevelCompletedDialog(int tempAccuracy) {
        if (game5Timer != null) {
            game5Timer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        lcf = LevelCompletedFragment.newInstance(tempAccuracy);
        lcf.setActivity(Game5_Bird.this);
        lcf.show(fm, "Johnny");
        sc.playWinSoundTrack(this);
    }

    private void showIntroductionDialog() {
        if (game5Timer != null) {
            game5Timer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }

        /**
         * starts the game directly if the dialog is set to never show
         */
        boolean shouldShowIntroDialog = SharedPrefsData.getShouldShowInstructDialog(getApplicationContext(), game_id);
        Log.d("Sunny", "shouldShowIntroDialog: " + String.valueOf(shouldShowIntroDialog));
        if (shouldShowIntroDialog) {
            introductionFragmentGame5Bird = IntroductionFragmentGame5Bird.newInstance(game_id);
            introductionFragmentGame5Bird.show(fm, "Introduction");
        }
    }

    private void uploadResult() {
        Log.d("Game5 bird: ", "uploadeResult()");
        if (resultUpload != null) {
            try {
                resultUpload.put("accuracy", GameDataHelper.getCorrectedAccuracy(accuracy))
                        .put("win", GameDataHelper.isWin(accuracy));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            StringEntity entity = new StringEntity(resultUpload.toString(), "UTF-8");

            try {
                APIConnection.sharedInstance.postArray(Game5_Bird.this, entity, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            Log.d("UploadResult", String.valueOf(response));
                            String error = String.valueOf(response.getString("error"));

                            switch (error) {
                                case "true":   //Fail to upload result
                                    generateGameReport(accuracy);
                                    Log.d("sunny", "Game5_Bird, "+ "Insert error: true ");
//                                    Toast.makeText(getApplicationContext(), "Insert error: true ", Toast.LENGTH_LONG).show();
                                    break;
                                case "false":   //Upload result successfully
                                    generateGameReport(accuracy);
                                    Log.d("sunny", "Game5_Bird, "+ "Insert error: False ");
//                                    Toast.makeText(getApplicationContext(), "Insert error : False ", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                        Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Fail to upload record", Toast.LENGTH_LONG).show();
                        Log.d("Game5_Bird-onFailure","onFailure without JSONARRAY");
                        progressDialog.dismiss();
                        Log.w("Status Code:", statusCode + "");
                    }

                    @Override
                    public void onStart() {
                        //When uploading the data, a dialog is shown
                        progressDialog = ProgressDialog.show(Game5_Bird.this, "Uploading the result to server...",
                                "Please wait..", true);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        long progressPercentage = (long) 100 * bytesWritten / totalSize;
                        progressDialog.setProgress((int) progressPercentage);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultUpload == null) {
            //If no word is clicked
//            generateGameReport(accuracy);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.sound) {
            matcher = pattern.matcher(audio);
            if (matcher.find()) {
                try {
                    System.out.println("Audio path used:" + matcher.group(1));
                    sc.playWordSound(matcher.group(1), Game5_Bird.this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (gcd.getGameStatus().equals("stop")) {
                gcd.setGameStatus("start");
                startGame();
                //This is for indicating that if the progress bar should animate when the player pause the game and click "RESUME" again
                //Notice: This method should be placed after startGame(), otherwise, it will invoke null pointer exception of game5Timer.
                game5Timer.setIsSoundButtonClicked(true);
                instruction.setVisibility(View.GONE);
                linear.setVisibility(View.VISIBLE);
            }
            return;
        }
        if (gcd.getGameStatus().equals("stop")) {
            return;
        }

        //check
        for (int i = 0; i < characters.length; i++) {
            for (int k = 0; k < characters[i].length; k++) {
                //Choose the right word, then add accuracy and next level
                if (v.equals(characters[i][k].getIb()) && characters[i][k].getOneCharacter().equals(answer.getOneCharacter())) {
                    //System.out.println(characters[i][k].getOneCharacter());
                    setImageButtonsClickable(characters,false);
                    playCorrectSound();
                    addAccuracy();
                    prepareUploadResult(characters[i][k].getOneCharacter(), i, k, 1,ans,ansrow,anscol);
                    nextTrial();
                }

                //Choose wrong word, then no accuracy added and next level
                if (v.equals(characters[i][k].getIb()) && !(characters[i][k].getOneCharacter().equals(answer.getOneCharacter()))) {
                    //System.out.println(characters[i][k].getOneCharacter());
                    setImageButtonsClickable(characters,false);
                    isWrongWordSelected = true;
                    playWrongSound();
                    prepareUploadResult(characters[i][k].getOneCharacter(), i, k, 0,ans,ansrow,anscol);
                    indicateTargetWord(ansrow,anscol);
                    goToNextTrial();
                }

            }
        }
//        checkCorrectness(v);
    }

    private void indicateTargetWord(int ansrow, int anscol){
        game5Timer.stop();

        final int tempAnsrow = ansrow;
        final int tempAnscol = anscol;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //stuff that updates ui
                animation.cancel();
                Character character = characters[tempAnsrow][tempAnscol];
                character.getIb().setBackgroundColor(getResources().getColor(R.color.orange));
                character.getIb().setClickable(true);
                character.getIb().setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gcd.setGameStatus("start");
                        nextTrial();
                    }
                });
            }
        });
    }

    private void goToNextTrial() {
        game5Timer.stop();

        if (game5Timer != null) {
            game5Timer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        animation.cancel();
                    }
                });
                gcd.setGameStatus("pause");
            }
        }
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                gcd.setGameStatus("start");
//                nextTrial();
//
//                Log.d("sunny", "delayed");
//
//            }
//        }, length_in_millisecond);
    }

    private void prepareUploadResult(String clickedWord, int row, int col, int isCorrect, String ans, int ansrow,int anscol) {
        if (resultUpload == null) {
            resultUpload = new JSONObject();
            String tempGameLevelId = GameDataHelper.getGameLevel_ID(GameDataHelper.GAME_3_ID,stage,playingLevel);
            Log.d("Game5Bird", "prepareUploadResult-- "+ "GameLevelID: "+ tempGameLevelId);

            try {
                resultUpload.put("game_id", GameDataHelper.GAME_3_ID)
                        .put("gamelevel_id", GameDataHelper.getGameLevel_ID(GameDataHelper.GAME_3_ID,stage,playingLevel))
                        .put("records", new JSONArray())
                        .put("startingTimeStamp", game5birdTimeStamp.getStartingTimeStamp());
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", game5Timer.getResponseTime())
                        .put("numOfRow", row + 1)
                        .put("numOfCol", col + 1)
                        .put("char", clickedWord)
                        .put("firsttime", true)
//                                .put("char2", "")
                        .put("outcome", isCorrect)
                        .put("ans", ans)
                        .put("ansrow", ansrow+1)
                        .put("anscol", anscol+1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1,matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONObject jsonObject1 = new JSONObject()
                        .put("trial", gcd.getTrial() + 1)
                        .put("responseTime", game5Timer.getResponseTime())
                        .put("numOfRow", row + 1)
                        .put("numOfCol", col + 1)
                        .put("char", clickedWord)
                        .put("firsttime", true)
//                                .put("char2", "")
                        .put("outcome", isCorrect)
                        .put("ans", ans)
                        .put("ansrow", ansrow+1)
                        .put("anscol", anscol+1);

                JSONObject merged = matrixPosition.getMergedJsonObject(jsonObject1,matrixPosition.getJsonObject());
                resultUpload.getJSONArray("records").put(merged);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.w("Game5 bird prepareUpload", resultUpload.toString());
    }

    private void playCorrectSound() {
        mediaPlayer = MediaPlayer.create(this, R.raw.correct);
        mediaPlayer.start();
    }

    private void playWrongSound() {
        mediaPlayer = MediaPlayer.create(this, R.raw.wrong);
        mediaPlayer.start();
    }

    private void addAccuracy() {
        accuracy = accuracy + 1;
//        Toast.makeText(getApplicationContext(), ""+accuracy, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (gcd.getGameStatus().equals("stop") && v != sound) {
            return false;
        }
        return TouchEvent.onTouch(v, event);
    }

    public void startAnimateProgressBar(int rate) {
        final int frate = rate;
        runOnUiThread(new Runnable() {
            public void run(){
                animation = ObjectAnimator.ofInt(progressBar, "progress", 0);
                animation.setDuration(frate);
                animation.setInterpolator(new LinearInterpolator());
                animation.start();
            }
        });
    }

    public void setProgressBar(int rate) {
        progressBar.setProgress(rate);
    }

    public void processGameEnd() {
//        if (game5Timer != null) {
//            game5Timer.stop();
//            if (gcd.getGameStatus().equals("start")) {
//                runOnUiThread(new Runnable() {
//                    public void run() {
//                        animation.cancel();
////                        generateGameReport(accuracy);
//                        uploadResult();
//                    }
//
//                });
//                gcd.setGameStatus("pause");
//            }
//        }
        isProcessGameEnd = true;

        //This is for indicating that if the progress bar should animate when the player pause the game and click "RESUME" again
        game5Timer.setIsSoundButtonClicked(false);
        setImageButtonsClickable(characters,false);
        prepareUploadResult("X", 0, 0, 0,ans,ansrow,anscol);
        indicateTargetWord(ansrow,anscol);
        if (gcd.getGameStatus().equals("stop")) {
            return;
        }

        if (game5Timer != null) {
            game5Timer.stop();
            if (gcd.getGameStatus().equals("start")) {
                runOnUiThread(new Runnable() {
                    public void run() {
//                        animation.cancel();
//                        generateGameReport(accuracy);
//                        uploadResult(); // generateGameReport() in the Async Task of this uploadResult()

                        sc.playWrongSound(Game5_Bird.this);
                        // The positions are filled with 0, but actually it is null eventually upload to server.

//                        nextTrial();
                        goToNextTrial();
                    }
                });
            }
        }
    }

    private void generateGameReport(double accuracy) {
        int correctedAccuracy = GameDataHelper.getCorrectedAccuracy(accuracy);
        boolean isWin = GameDataHelper.isWin(accuracy);
        if (!isWin) {
            showLevelFailedDialog(correctedAccuracy);
        }
        if (isWin) {
            showLevelCompletedDialog(correctedAccuracy);
        }
//        Intent i = new Intent(getApplicationContext(), GameReport.class);
//        i.putExtra("CALL_FROM", "Game5_Bird");
//        i.putExtra("LEVEL", gcd.getTrial() + 1);    //+1 because the level 0 in gcd actually represents level 1
//
//        accuracy = (accuracy / gcd.getTrial()) * 100;    //Calculate the accuracy
//        Log.w("GAME1 GAMEREPORT", "accuracy:  "+ accuracy);
//        i.putExtra("ACCURACY", accuracy);
//        startActivity(i);
//        finish();
    }

    public void startGame() {
        if (game5Timer == null) {
            game5Timer = new Game5Timer(gcd);
            game5Timer.setActivity(this);
        }
        game5Timer.start();
    }

    public void resumeGame() {
        if(game5Timer.getIsSoundButtonClicked()) {
            game5Timer.resume();
        } else {
            //Although we don't resume the game, we need to set the game status to be "stop" for sound button to fit the if statement (if gcd.getGameStatus("stop") { startNextTrial... })
            gcd.setGameStatus("stop");
        }
    }

    public void stopGame() {
        game5Timer.stop();
    }

    public void nextTrial() {
        if (gcd.getGameStatus().equals("start")) {
            System.out.println("next");
            animation.cancel();
            if (gcd.nextTrial()) {
                game5Timer.updateTrial();
            } else {
                //When the player loses or all the trials are finished, a report will be generated.
//                generateGameReport(accuracy);
//                System.out.println("no more level");
                Log.d("Game5_bird upload result", "NO MORE LEVEL");
                sound.setClickable(false);
                uploadResult();
            }
        }
    }

    private void setImageButtonsClickable(Character[][] characters, boolean clickable) {
        if (characters != null) {
            for (Character[] characters1 : characters) {
                for (Character character : characters1) {
                    TextView textView = character.getIb();
                    if (clickable){
                        textView.setOnClickListener(this);
                    }
                    textView.setClickable(clickable);

                }
            }
        }
        Log.d("sunny", "null characters"+ "clickable: "+ clickable);
    }


    public void checkCorrectness(View v) {
        for (int i = 0; i < characters.length; i++) {
            for (int k = 0; k < characters[i].length; k++) {
                if (v == characters[i][k].getIb() && characters[i][k].getOneCharacter() == String.valueOf(audio)) {
                    System.out.println(characters[i][k].getOneCharacter());
                    nextTrial();
                }
            }
        }
    }

    //When the introduction dialog is showing: if the user press the back pressed button
    //nothing happen because this game need to wait the user press the animal so that
    //the game can start
    @Override
    public void onItemClicked() {
        //handled events when the introFragment is detached
    }

    //When the fail Fragment dialog is showing: if the user press the back pressed button
    //the activity will finish itself and go back to game level menu.
    @Override
    public void failFragmentOnItemClicked() {
        finish();
    }

    @Override
    public void levelCompletedFragmentOnItemClicked() {
        finish();
    }



    public void unbindDrawables(View view) {//pass your parent view here
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            }else if(view instanceof TextView){
                TextView textView = (TextView) view;
                textView.setBackground(null);

            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onTrimMemory(int level) {

        // Determine which lifecycle or system event was raised.
        switch (level) {

            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                break;

            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                break;

            default:
                break;
        }
    }
}