package appslab.chinesecharacteracquisitiongame.gamescene;

import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ted on 25/5/2016.
 */
public class TouchEvent {

    public static boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            v.setAlpha(0.5f);
        }else{
            v.setAlpha(1.0f);
        }
        return false;
    }
}
