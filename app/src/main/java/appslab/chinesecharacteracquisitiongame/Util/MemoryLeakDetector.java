package appslab.chinesecharacteracquisitiongame.Util;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by SunnyChan on 24/11/2016.
 */

public class MemoryLeakDetector extends Application {

    @Override public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...
    }
}
