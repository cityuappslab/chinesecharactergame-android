package appslab.chinesecharacteracquisitiongame.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.PreferenceManager;
import android.util.Log;

import com.blankj.utilcode.utils.NetworkUtils;

import java.util.Map;

/**
 * Created by SunnyChan on 10/8/2016.
 */
public class GeneralUtil {
    public static final Boolean DEBUG_USE_GLIDE = true;
    public static final Boolean DEBUG = false;
    public static final Boolean DEBUG_AUTO_LOGIN = true;
    public static final String DEBUG_LOGIN_MAIL = "test03@gmail.com";
    public static final String DEBUG_LOGIN_PW = "admina";

    public static final boolean DEBUG_TEST_LEVEL = false;
    public static final int TEST_LEVEL = 40;

    public static final boolean DEBUG_TEST_STAGE = false;
    public static final int TEST_STAGE = 1;

    //setting the is win as true so that can quickly run through all the levels
    public static final boolean DEBUG_TEST_OVERALL = false;
    public static final boolean DEBUG_IS_WIN = false;
//    public static final double DEBUG_ACCURACY = 90;

    public static void getAllSharedPrefsKeys(Context context){
        Map<String, ?> allEntries = PreferenceManager.getDefaultSharedPreferences(context).getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    public static void loggingHelper(Context context, String tag, String message) {
        Log.d(context.getClass().getSimpleName(), tag + ": " + message);
    }

    public static void showAlertDialog_NetworkConnection(final Activity activity, String message){
        final Context context = activity;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton("Enable network", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                NetworkUtils.openWirelessSettings(context);
                activity.finish();
            }
        });
        builder.show();
    }

}
