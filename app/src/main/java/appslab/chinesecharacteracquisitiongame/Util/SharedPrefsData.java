package appslab.chinesecharacteracquisitiongame.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by SunnyChan on 11/8/2016.
 */
public class SharedPrefsData {
    //pref keys
    public static final String STAGE_NUMBER = "STAGE_NUMBER";
    public static final String PREFS_FILE = "PREF";
    public static final String PREFS_FILE_KEEP = "PREF_KEEP";
    public static final String SHOULD_SHOW = "SHOULD_SHOW";
    public static final String GAME_LEVEL_UNLOCKED = "GAME_LEVEL_UNLOCKED";
    public static final String SHOULD_SHOW_STAGE_COMPLETED_DIALOG = "SHOW_SHOW_STAGE_COMPLETED";
    public static final String SHOULD_SHOW_STAGE_COMPLETED_DIALOG_SERVER = "SHOW_SHOW_STAGE_COMPLETED_SERVER";
    //used for storing the info in registor
    public static final String REGISTER_INFO = "REGISTER_INFO";
    public static final String REGISTER_INFO_NAME = "REGISTER_INFO_NAME";
    public static final String REGISTER_INFO_EMAIL = "REGISTER_INFO_EMAIL";
    public static final String REGISTER_INFO_REGCODE= "REGISTER_INFO_REGCODE";
    public static final String REGISTER_INFO_ISSUCCESSFUL = "REGISTER_INFO_ISSUCCESSFUL";
    public static final String LOGIN_INFO_EMAIL = "LOGIN_INFO_EMAIL";


    public static void clearSharedPrefernce(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        settings.edit().clear().apply();
    }

    //for setting the name, email, regCode in SharedPrefs for retrieving later
    public static void setRegisterInfo(Context context, String name, String email, String regCode){
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        settings.edit().putString(REGISTER_INFO_NAME, name).apply();
        settings.edit().putString(REGISTER_INFO_EMAIL, email).apply();
        settings.edit().putString(REGISTER_INFO_REGCODE, regCode).apply();
    }

    /** for more convenient login */
    public static void setLoginEmail(Context context, String email){
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE_KEEP, 0);
        settings.edit().putString(LOGIN_INFO_EMAIL, email).apply();
    }

    public static String getLoginEmail(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE_KEEP,0);
        return settings.getString(LOGIN_INFO_EMAIL,"");
    }

    public static String getRegisterInfoName(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE,0);
        return settings.getString(REGISTER_INFO_NAME,"");
    }

    public static String getRegisterInfoEmail(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE,0);
        return settings.getString(REGISTER_INFO_EMAIL,"");
    }

    public static String getRegisterInfoRegcode(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE,0);
        return settings.getString(REGISTER_INFO_REGCODE,"");
    }

    public static void setRegisterInfoIsSuccessful(Context context, boolean isSuccessful){
        SharedPreferences setting = context.getSharedPreferences(PREFS_FILE, 0);
        setting.edit().putBoolean(REGISTER_INFO_ISSUCCESSFUL, isSuccessful).apply();
    }
    public static boolean getRegisterInfoIsSuccessful(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE,0);
        return settings.getBoolean(REGISTER_INFO_ISSUCCESSFUL,true);
    }

    //TODO for storing the record from server
    public static void setShouldShowCompletedDialog_Server(Context context, boolean shouldShow,int stage_num){
        int stage_num_temp = stage_num;
        boolean shouldShowStageCompleted = shouldShow;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
//        checkShouldShowStageCompletedDialog(context,stage_num);
        settings.edit().putBoolean(SHOULD_SHOW_STAGE_COMPLETED_DIALOG_SERVER+stage_num_temp,shouldShowStageCompleted).apply();
        Log.d("sunny", "setShouldShowStageCompletedDialog_Server: " + shouldShowStageCompleted);
    }

    public static boolean getShouldShowStageCompletedDialog_Server(Context context ){
        boolean shouldShowStageCompleted = false;
        int stage_num = SharedPrefsData.getStageNumber(context);
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        shouldShowStageCompleted = settings.getBoolean(SHOULD_SHOW_STAGE_COMPLETED_DIALOG_SERVER+stage_num, false);
        Log.d("sunny", "getShouldShowStageCompletedDialog_Server: " + shouldShowStageCompleted);
        return shouldShowStageCompleted;
    }

    public static boolean getShouldShowStageCompletedDialog(Context context) {
        boolean shouldShowStageCompleted = false;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        shouldShowStageCompleted = settings.getBoolean(SHOULD_SHOW_STAGE_COMPLETED_DIALOG, false);
        Log.d("sunny", "getShouldShowStageCompletedDialog: " + shouldShowStageCompleted);
        boolean shouldShowStageCompleted_server = SharedPrefsData.getShouldShowStageCompletedDialog_Server(context);
        //also need to compare with the data stored in server
        shouldShowStageCompleted = shouldShowStageCompleted && shouldShowStageCompleted_server;
        return shouldShowStageCompleted;
    }
    public static void setShouldShowStageCompletedDialog(Context context,boolean shouldShow) {
        boolean shouldShowStageCompleted = shouldShow;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        settings.edit().putBoolean(SHOULD_SHOW_STAGE_COMPLETED_DIALOG,shouldShowStageCompleted).apply();
        Log.d("sunny", "setShouldShowStageCompletedDialog: " + shouldShowStageCompleted);
    }

    //use to compare the change in the stage_number, if stage_number is changed, levelUp_dialog and stage_completed_dialog should be shown.
    private static void checkShouldShowStageCompletedDialog(Context context, int server_stage_number) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        int local_stage_number = getStageNumber(context);
        Log.d("sunny", "checkShouldShowStageCompletedDialog: " + ", local_stage_number: " + local_stage_number + ", server_stage_number: " + server_stage_number);
        if ((local_stage_number != server_stage_number)) {
            editor.putBoolean(SHOULD_SHOW_STAGE_COMPLETED_DIALOG, true).apply();
        }
        Log.d("sunny", "checkShouldShowStageCompletedDialog: "+getShouldShowStageCompletedDialog(context));
    }

    public static int getStageNumber(Context context) {
        int defaultStageNumber = (GeneralUtil.DEBUG_TEST_STAGE) ? GeneralUtil.TEST_STAGE : 0;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        int stage_number = settings.getInt(STAGE_NUMBER, defaultStageNumber);
        Log.d("sunny", "getStageNumber, "+"defaultStageNumber: "+ defaultStageNumber);
        return stage_number;
    }

    public static void setStageNumber(Context context, int stage_number) {
        int tempStageNumber = (GeneralUtil.DEBUG_TEST_STAGE) ? GeneralUtil.TEST_STAGE : stage_number;
        //do a checking
        checkShouldShowStageCompletedDialog(context,tempStageNumber);
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        settings.edit().putInt(STAGE_NUMBER, tempStageNumber).apply();
    }
    public static void setStageNumber_debug(Context context, int stage_number) {
        int tempStageNumber = stage_number;
        //do a checking
        checkShouldShowStageCompletedDialog(context,tempStageNumber);
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        settings.edit().putInt(STAGE_NUMBER, tempStageNumber).apply();
    }

    public static boolean getShouldShowInstructDialog(Context context, int game_id) {
        String ShouldShow = SHOULD_SHOW+game_id;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE_KEEP, 0);
        boolean shouldShowDialog = settings.getBoolean(ShouldShow, true);
        return shouldShowDialog;
    }

    public static void setShouldShowInstructDialog(Context context, boolean isChecked, int game_id) {
        String ShouldShow = SHOULD_SHOW+game_id;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE_KEEP, 0);
        settings.edit().putBoolean(ShouldShow, isChecked).apply();
    }

    /**
     * @param context
     * @param game_id 11:game1, 12:game4, 13:game5_bird, 14:game5_bat
     *                //     * @param stageNumber
     * @return return the corresponding game level of each game
     */
    public static int getGameLevel(Context context, int game_id) {
        int default_game_level = GeneralUtil.DEBUG_TEST_LEVEL ? GeneralUtil.TEST_LEVEL : 1;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        String pref_location = GAME_LEVEL_UNLOCKED + game_id;
        int gameLevelUnlocked = settings.getInt(pref_location, default_game_level);
        return gameLevelUnlocked;
    }

    public static void setGameLevel(Context context, int game_id, int currentLevel) {
        int tempLevel = (GeneralUtil.DEBUG_TEST_LEVEL) ? GeneralUtil.TEST_LEVEL : currentLevel;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        String pref_location = GAME_LEVEL_UNLOCKED + game_id;
        settings.edit().putInt(pref_location, tempLevel + 1).apply();
    }

    public static void setGameLevel_debug(Context context, int game_id, int currentLevel) {
        int tempLevel = currentLevel;
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        String pref_location = GAME_LEVEL_UNLOCKED + game_id;
        settings.edit().putInt(pref_location, tempLevel + 1).apply();
    }
}