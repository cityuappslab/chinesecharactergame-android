package appslab.chinesecharacteracquisitiongame.dialogs;


import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import appslab.chinesecharacteracquisitiongame.R;

public class LevelFailedFragment extends android.app.DialogFragment {
    FailFragmentOnItemClickedListener mListener = sDummyCallbacks;
    static int accuracy;
    TextView accuracyTV;
    ImageButton menuIB, retryIB;
    Activity activity;

    public LevelFailedFragment() {
        // Required empty public constructor
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    public static LevelFailedFragment newInstance(int _accuracy) {
        LevelFailedFragment fragment = new LevelFailedFragment();
        accuracy = _accuracy;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
        View view = inflater.inflate(R.layout.dialog_level_failed, container);
        findViewById(view);
        initializeUI();
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //start the timer when this fragment is detached
        mListener.failFragmentOnItemClicked();
    }

    private void initializeUI() {
        accuracyTV.setText(accuracy + "%");
        getDialog().setCanceledOnTouchOutside(false);

        retryIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();
                activity.finish();
            }
        });
    }

    private void findViewById(View view) {
        accuracyTV = (TextView) view.findViewById(R.id.accuracy);
//        menuIB = (ImageButton) view.findViewById(R.id.menu);
        retryIB = (ImageButton) view.findViewById(R.id.retry);
    }

    public void setActivity(Activity _activity) {
        activity = _activity;
    }

    private static FailFragmentOnItemClickedListener sDummyCallbacks = new FailFragmentOnItemClickedListener() {
        @Override
        public void failFragmentOnItemClicked() {

        }
    };

    // Put this interface on the Fragment or the View
    public interface FailFragmentOnItemClickedListener {
        void failFragmentOnItemClicked();
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.lv_completed_dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.lv_completed_dialog_height);
        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (LevelFailedFragment.FailFragmentOnItemClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnItemClickedListener");
        }
    }
}
