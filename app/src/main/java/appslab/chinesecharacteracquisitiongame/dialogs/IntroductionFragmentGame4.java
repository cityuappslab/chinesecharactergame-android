package appslab.chinesecharacteracquisitiongame.dialogs;

import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;

/**
 * Created by SunnyChan on 12/8/2016.
 */
public class IntroductionFragmentGame4 extends android.app.DialogFragment {
    OnItemClickedListener mListener = sDummyCallbacks;
    ImageButton btnClose;
    CheckBox checkBox;

    private static int game_id;

    public IntroductionFragmentGame4() {
        // Required empty public constructor
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    public static IntroductionFragmentGame4 newInstance(int game_id_temp) {
        IntroductionFragmentGame4 fragment = new IntroductionFragmentGame4();
        game_id = game_id_temp;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
        final View view = inflater.inflate(R.layout.dialog_introduction_game_4, container);

        checkBox = (CheckBox) view.findViewById(R.id.instruct_checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPrefsData.setShouldShowInstructDialog(getActivity().getApplicationContext(),!isChecked,game_id);
            }
        });

        btnClose = (ImageButton) view.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFragment();
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //start the timer when this fragment is detached
        mListener.onItemClicked();
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.lv_completed_dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.lv_completed_dialog_height);
        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnItemClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnItemClickedListener");
        }
    }

    private static OnItemClickedListener sDummyCallbacks = new OnItemClickedListener() {
        @Override
        public void onItemClicked() {

        }
    };

    // Put this interface on the Fragment or the View
    public interface OnItemClickedListener {
        void onItemClicked();
    }

    private void closeFragment() {
        getActivity().getFragmentManager().beginTransaction().remove(this).commit();
    }
}