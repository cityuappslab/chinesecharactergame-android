package appslab.chinesecharacteracquisitiongame.dialogs;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import appslab.chinesecharacteracquisitiongame.R;
import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;
import appslab.chinesecharacteracquisitiongame.networkconnection.APIConnection;
import cz.msebera.android.httpclient.Header;

/**
 * Created by SunnyChan on 22/8/2016.
 */
public class StageCompletedFragment extends android.app.DialogFragment {
    OnItemClickedListener mListener = sDummyCallbacks;
    ImageView iv_lvUp;

    public StageCompletedFragment() {
        // Required empty public constructor
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);

    }

    public static StageCompletedFragment newInstance() {
        StageCompletedFragment fragment = new StageCompletedFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
        final View view = inflater.inflate(R.layout.dialog_stage_completed, container);
        iv_lvUp = (ImageView) view.findViewById(R.id.stage_completed_animal_image);
        setLVUPIcon();

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //start the timer when this fragment is detached
        mListener.onItemClicked();
        if (!GeneralUtil.DEBUG_TEST_STAGE)
            SharedPrefsData.setShouldShowStageCompletedDialog(getActivity().getApplicationContext(), false);
        int stage_number = SharedPrefsData.getStageNumber(getActivity().getApplicationContext());
        Log.d("sunny", "stageCompletedFragement: "+"stage_number: "+stage_number);
        postShownDialog(stage_number);
    }

    private void postShownDialog(int stage_number){
        final Context mContext = getActivity();

        RequestParams params = new RequestParams();
        params.put("stage", stage_number);
        params.put("shown", 1);

        APIConnection.getInstance().postStageCompletedDiaglog(params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    String error = String.valueOf(response.getString("error"));
                    if (error == "true") {  //Get Game Data unsuccessfully
                        Toast.makeText(getActivity(), "Fail to post the Game Data. ", Toast.LENGTH_LONG).show();
                    } else if (error == "false") {  //Post successfully
                        Log.d("sunny", "StageCompletedFragment: " + "post successful");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Toast.makeText(getApplicationContext(), "onFailure without JSONARRAY", Toast.LENGTH_LONG).show();
                Log.d(this.getClass().getSimpleName() + "-onFailure", String.valueOf(statusCode));
                Log.d(this.getClass().getSimpleName() + "-onFailure", responseString);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.stage_completed_dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.stage_completed_dialog_height);
        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnItemClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnItemClickedListener");
        }
    }

    private static OnItemClickedListener sDummyCallbacks = new OnItemClickedListener() {
        @Override
        public void onItemClicked() {

        }
    };

    // Put this interface on the Fragment or the View
    public interface OnItemClickedListener {
        void onItemClicked();
    }

    private void closeFragment() {
        getActivity().getFragmentManager().beginTransaction().remove(this).commit();
    }


    private void setLVUPIcon() {
        int stage_number = SharedPrefsData.getStageNumber(getActivity().getApplicationContext());
        switch (stage_number) {
            case 0://egg
                iv_lvUp.setImageResource(R.drawable.lvup_dialog_animal1);
                break;
            case 1://cracked egg
                iv_lvUp.setImageResource(R.drawable.lvup_dialog_animal2);
                break;
            case 2://little chick
                iv_lvUp.setImageResource(R.drawable.lvup_dialog_animal3);
                break;
            case 3://mother chick
                iv_lvUp.setImageResource(R.drawable.lvup_dialog_animal4);
                break;
            default:
                Log.d("sunny", "setLVUPIcon, " + "unhandled stageNumber: " + stage_number);
                iv_lvUp.setImageResource(R.drawable.lvup_dialog_animal1);
                break;
        }
    }
}
