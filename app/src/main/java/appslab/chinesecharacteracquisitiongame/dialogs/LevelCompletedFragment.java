package appslab.chinesecharacteracquisitiongame.dialogs;


import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import appslab.chinesecharacteracquisitiongame.R;

public class LevelCompletedFragment extends android.app.DialogFragment {
    LevelCompletedFragmentOnItemClickedListener mListener = sDummyCallbacks;
    static int accuracy;
    TextView accuracyTV;
    ImageButton continueIB;
    Activity activity;


    public LevelCompletedFragment() {
        // Required empty public constructor
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    public static LevelCompletedFragment newInstance(int _accuracy) {
        LevelCompletedFragment fragment = new LevelCompletedFragment();
        accuracy = _accuracy;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
        View view = inflater.inflate(R.layout.dialog_level_completed, container);
        findViewById(view);
        initializeUI();
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //start the timer when this fragment is detached
        mListener.levelCompletedFragmentOnItemClicked();
    }

    private static LevelCompletedFragmentOnItemClickedListener sDummyCallbacks = new LevelCompletedFragmentOnItemClickedListener() {
        @Override
        public void levelCompletedFragmentOnItemClicked() {

        }
    };

    // Put this interface on the Fragment or the View
    public interface LevelCompletedFragmentOnItemClickedListener {
        void levelCompletedFragmentOnItemClicked();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (LevelCompletedFragment.LevelCompletedFragmentOnItemClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnItemClickedListener");
        }
    }

    private void initializeUI() {
        accuracyTV.setText(accuracy + "%");
        getDialog().setCanceledOnTouchOutside(false);

        continueIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();
                activity.finish();

            }
        });
    }

    private void findViewById(View view) {
        accuracyTV = (TextView) view.findViewById(R.id.accuracy);
        continueIB = (ImageButton) view.findViewById(R.id.continue_);
    }

    public void setActivity(Activity _activity) {
        activity = _activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.lv_completed_dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.lv_completed_dialog_height);
        getDialog().getWindow().setLayout(width, height);
    }
}
