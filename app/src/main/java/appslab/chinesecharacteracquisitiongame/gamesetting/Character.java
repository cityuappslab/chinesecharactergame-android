package appslab.chinesecharacteracquisitiongame.gamesetting;

import android.widget.TextView;

/**
 * Created by appslab on 6/4/16.
 */
public class Character {
    private String oneCharacter;
    private String image_url;
    private String audio_url;
    private TextView ib;
    public Character(String oneCharacter, String image_url, String audio_url) {
        this.oneCharacter = oneCharacter;
        this.image_url = image_url;
        this.audio_url = audio_url;
    }

    public Character() {
    }

    public String getOneCharacter() {
        return oneCharacter;
    }

    public void setOneCharacter(String oneCharacter) {
        this.oneCharacter = oneCharacter;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getAudio_url() {
        return audio_url;
    }

    public void setAudio_url(String audio_url) {
        this.audio_url = audio_url;
    }

    public TextView getIb() {
        return ib;
    }

    public void setIb(TextView ib) {
        this.ib = ib;
    }

}
