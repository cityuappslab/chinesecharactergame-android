package appslab.chinesecharacteracquisitiongame.gamesetting;

/**
 * Created by ted on 25/5/2016.
 */
public interface GameController {
    public void startAnimateProgressBar(int rate);
    public void setProgressBar(int rate);
    public void processGameEnd();
    public void startGame();
    public void resumeGame();
    public void stopGame();
    public void nextTrial();
    public void getCoreData();
    public void createCharacter();
    public void removeTargetWord();
}
