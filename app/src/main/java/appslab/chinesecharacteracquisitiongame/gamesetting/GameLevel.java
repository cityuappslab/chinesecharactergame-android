package appslab.chinesecharacteracquisitiongame.gamesetting;
/**
 * Created by Johnny on 7/19/16.
 */
public class GameLevel {
    private int gamelevel_id;
    private int level;
    private double duration;
    private int characterSet_id;
    private int numOfCol;
    private int getNumOfRow;
    private double text_disappear_time;
    private int decrease_percentage;

    public GameLevel(int gamelevel_id, int level, double duration, int characterSet_id, int numOfCol, int getNumOfRow) {
        this.gamelevel_id = gamelevel_id;
        this.level = level;
        this.duration = duration;
        this.characterSet_id = characterSet_id;
        this.numOfCol = numOfCol;
        this.getNumOfRow = getNumOfRow;
    }

    public int getGamelevel_id() {
        return gamelevel_id;
    }

    public void setGamelevel_id(int gamelevel_id) {
        this.gamelevel_id = gamelevel_id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public int getCharacterSet_id() {
        return characterSet_id;
    }

    public void setCharacterSet_id(int characterSet_id) {
        this.characterSet_id = characterSet_id;
    }

    public int getNumOfCol() {
        return numOfCol;
    }

    public void setNumOfCol(int numOfCol) {
        this.numOfCol = numOfCol;
    }

    public int getGetNumOfRow() {
        return getNumOfRow;
    }

    public void setGetNumOfRow(int getNumOfRow) {
        this.getNumOfRow = getNumOfRow;
    }

    public double getText_disappear_time() {
        return text_disappear_time;
    }

    public void setText_disappear_time(double text_disappear_time) {
        this.text_disappear_time = text_disappear_time;
    }

    public int getDecrease_percentage() {
        return decrease_percentage;
    }

    public void setDecrease_percentage(int decrease_percentage) {
        this.decrease_percentage = decrease_percentage;
    }
}

