package appslab.chinesecharacteracquisitiongame.gamesetting;
import android.app.Activity;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ted on 25/5/2016.
 */
public class GameTimer
{
    Activity activity;
    Double duration;
    Double decreaseTime = 5.0;
    int period = 5;
    Double TotalTime;
    Double responseTimeCounter = 0.0;
    GameCoreData gcd;
    Timer timer;
    long currentSystemTime;

    public GameTimer(GameCoreData gcd){
        timer = new Timer(true);
        duration = gcd.getDurationFromTrial();
        TotalTime = duration;
        this.gcd = gcd;
        //Reset the responseTimeCounter to 0.0 second
        responseTimeCounter = 0.0;
    }

    public void start(){
        timer = new Timer(true);
//        Log.d("timer", "GameTimer start method, Current system time:" + (currentSystemTime = System.currentTimeMillis()));
        ((GameController)activity).startAnimateProgressBar(Integer.valueOf(TotalTime.intValue()));
        timer.schedule(new Task(), 0, period);
        gcd.setGameStatus("start");
    }

    public void resume(){
        timer = new Timer(true);
        timer.schedule(new Task(), 0, period);
        ((GameController)activity).startAnimateProgressBar(Integer.valueOf(TotalTime.intValue()));
        gcd.setGameStatus("start");
    }

    public void stop(){timer.cancel();}

    public void updateTrial() {
        stop();
        duration = gcd.getDurationFromTrial();
        TotalTime = duration;

        responseTimeCounter = 0.0;
        ((GameController)activity).setProgressBar(100);
        ((GameController)activity).createCharacter();
        ((GameController)activity).startGame();
    }

    public double getTime() {
        return TotalTime;
    }

    public double getResponseTime() {
        return responseTimeCounter;
    }

    public void setActivity(Activity a){this.activity=a;}
    public class Task extends TimerTask
    {
        public void run() {
            TotalTime -= decreaseTime;
            responseTimeCounter += decreaseTime;
//            Log.d("timer", "Total time:" +TotalTime + "\tdifference: "+ System.currentTimeMillis());


            if (TotalTime == 0) {
                ((GameController)activity).processGameEnd();
//                Log.d("timer", "*** Last Total time:" +TotalTime + "\t Total time according to System time: "+ (System.currentTimeMillis() - currentSystemTime));
                this.cancel();
            }
        }
    }
}
