package appslab.chinesecharacteracquisitiongame.gamesetting;

/**
 * Created by appslab on 6/4/16.
 */
public class GameTrial {
    private int trial;
    private double duration;

    public int getTrial() {
        return trial;
    }

    public void setTrial(int trial) {
        this.trial = trial;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

}
