package appslab.chinesecharacteracquisitiongame.gamesetting;

/**
 * Created by developer on 9/1/16.
 */
public interface TimeStamp {
    void createStartingTimeStamp();
    String getStartingTimeStamp();
}
