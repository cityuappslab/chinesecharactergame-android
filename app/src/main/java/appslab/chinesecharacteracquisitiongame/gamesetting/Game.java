package appslab.chinesecharacteracquisitiongame.gamesetting;

/**
 * Created by appslab on 6/4/16.
 */
public class Game {
    private int id;
    private String gameName;
    private String description;
    private String created_at;
    private String updated_at;

    public Game() {
    }

    public Game(int id, String gameName, String description, String created_at, String updated_at) {
        this.id = id;
        this.gameName = gameName;
        this.description = description;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
