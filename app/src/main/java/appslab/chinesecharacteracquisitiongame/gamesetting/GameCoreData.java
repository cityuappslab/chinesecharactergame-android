package appslab.chinesecharacteracquisitiongame.gamesetting;

import android.util.Log;

import java.util.ArrayList;
/**
 * Created by ted on 25/5/2016.
 */
public class GameCoreData {
    String gameStatus="stop";
    int trial = 0;
    ArrayList<GameTrial> trials = new ArrayList<GameTrial>();

    public String getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public int getTrial() {
        return trial;
    }

    public void setTrial(int trial) {
        this.trial = trial;
    }

    public ArrayList<GameTrial> getTrials() {
        return trials;
    }

    public void setTrials(ArrayList<GameTrial> trials) {
        this.trials = trials;
    }

    public void addTrial(GameTrial trial){
        trials.add(trial);
//        Log.d("GameCoreData", "Level added!");
    }

    public Boolean nextTrial(){
        setGameStatus("stop");
        if(trial +1< trials.size()) {
            trial += 1;
            Log.w("GCD LEVEL:", trial +"");
            return true;
        }
        return false;
    }

    public Double getDurationFromTrial(){
        return trials.get(trial).getDuration();
    }

}
