package appslab.chinesecharacteracquisitiongame.gamesetting;
import android.app.Activity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ted on 25/5/2016.
 */
public class WordTimer
{
    Activity activity;
    Double duration;
    Double decreaseTime=100.0;
    Double accumulatedDecreaseTime = 0.0;
    Double disappearTime;
    Double TotalTime;
    Timer timer;

    public WordTimer(GameTimer gameTimer, Double disappearTime){
        timer = new Timer(true);
        TotalTime = gameTimer.getTime();
        this.disappearTime = disappearTime;
    }

    public void start(){
        timer = new Timer(true);
        timer.schedule(new Task(), 0, 100);
    }

    public void resume(){
        timer = new Timer(true);
        timer.schedule(new Task(), 0, 100);
    }

    public void stop(){timer.cancel();}

    public double getTime() {
        return TotalTime;
    }

    public void setActivity(Activity a){this.activity=a;}

    public class Task extends TimerTask
    {
        public void run() {
            accumulatedDecreaseTime += decreaseTime;
            TotalTime -= decreaseTime;

            if (accumulatedDecreaseTime >= disappearTime) {
                ((GameController)activity).removeTargetWord();
                this.cancel();
            }
        }
    }
}
