package appslab.chinesecharacteracquisitiongame.gamesetting;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by developer on 9/1/16.
 */
public class Game1TimeStamp implements TimeStamp {
    Long startingTime = null;

    @Override
    public void createStartingTimeStamp() {
        startingTime = System.currentTimeMillis() / 1000;
    }

    @Override
    public String getStartingTimeStamp() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateTime = dateFormat.format(new Date()); // Find today's date
            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
