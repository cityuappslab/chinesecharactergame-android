package appslab.chinesecharacteracquisitiongame.gamesetting;

import android.content.Context;
import android.util.Log;

import appslab.chinesecharacteracquisitiongame.Util.GeneralUtil;
import appslab.chinesecharacteracquisitiongame.Util.SharedPrefsData;

/**
 * Created by SunnyChan on 17/8/2016.
 */
public class GameDataHelper {
    private static final String TAG = GameDataHelper.class.getSimpleName();
    public static final int GAME_1_ID = 11;
    public static final int GAME_2_ID = 12;
    public static final int GAME_3_ID = 13;
    public static final int GAME_4_ID = 14;

    public static final int STAGE_2_LEVEL_ADDITION = 20;
    public static final int STAGE_3_LEVEL_ADDITION = 40;



    public static Boolean isWin(double accuracy) {
        boolean isWin = false;
        int correctedAccuracy = getCorrectedAccuracy(accuracy);
        if (correctedAccuracy < 90) {
            isWin = false;
        }

        if (correctedAccuracy >= 90) {
            isWin = true;
        }

        isWin =  (GeneralUtil.DEBUG_TEST_OVERALL)? GeneralUtil.DEBUG_IS_WIN:isWin;
        loggingHelper("isWin", String.valueOf(isWin));
        return isWin;
    }

    public static int getCorrectedAccuracy(double accuracy) {
        int correctedAccuracy;
        correctedAccuracy = (int) Math.round((accuracy / 20) * 100);
        loggingHelper("getCorrectedAccuracy", String.valueOf(correctedAccuracy));
        return correctedAccuracy;
    }

    private static void loggingHelper(String tag, String message) {
        Log.d(TAG, tag + ": " + message);
    }

    /**
     * use for general check of the game level and return the corresponding value for game levels to unlock
     *
     * @param context
     * @param game_id     input the game_id(11-14)
     *                    //     * @param currentLevel ranging from 1-60
     * @param stageNumber ranging from 1-3
     * @return return the checked level
     */
    public static int getGameLevel(Context context, int game_id, int stageNumber) {
        int currentLevel = SharedPrefsData.getGameLevel(context, game_id);
        int server_stageNumber = SharedPrefsData.getStageNumber(context);
        loggingHelper("getGameLevel " + "sunny: ", String.valueOf(currentLevel) + ", stageNumber: " + stageNumber + ", game_id: " + game_id + ", serverStage: " + server_stageNumber);

        int levelReturned = 0;
        switch (stageNumber) {
            case 1:
                if (currentLevel > 20) {
                    levelReturned = 20;
                    break;
                } else {
                    levelReturned = currentLevel;
                    break;
                }
            case 2:
                if (currentLevel > 40) {
                    if (server_stageNumber>0) {
                        levelReturned = 20;
                        break;
                    }else{
                        levelReturned = 0;
                        break;
                    }
                } else if (currentLevel > 20 && currentLevel <= 40) {
                    //unlock the first level of a game if the server returns the stageNumber as 1
                    if (server_stageNumber > 0) {
                        levelReturned = currentLevel - 20;
                        break;
                    } else if (server_stageNumber <= 0) {
                        levelReturned = 0;
                        break;
                    }
                } else {
                    levelReturned = 0;
                    break;
                }
            case 3:
                if (currentLevel > 60) {
                    if (server_stageNumber>1) {
                        levelReturned = 20;
                        break;
                    }else{
                        levelReturned = 0;
                        break;
                    }
                } else if (currentLevel > 40 && currentLevel <= 60) {
                    //unlock the first level of a game if the server returns the stageNumber as 1
                    if (server_stageNumber > 1) {
                        levelReturned = currentLevel - 40;
                        break;
                    } else if (server_stageNumber <= 1) {
                        levelReturned = 0;
                        break;
                    }
                } else {
                    levelReturned = 0;
                    break;
                }
            default:
                Log.d(TAG, "unhandled level number: " + String.valueOf(currentLevel));
                levelReturned = currentLevel;
                break;
        }
        Log.d(TAG, "sunny " + "levelReturned: " + levelReturned);
        return levelReturned;
    }


    /**
     * @param game_id      11-14
     * @param stage        1-3
     * @param playingLevel 1-20
     * @return corrected gameLevel_id
     */
    public static String getGameLevel_ID(int game_id, int stage, int playingLevel) {
        int stageInt = stage;
        int playingInt = playingLevel;
        int gameLevel_ID = 0;

        switch (game_id) {
            case GAME_1_ID: //541-600
//                gameLevel_ID = 540 + _getGameLevel_ForGameLevel_ID(stageInt, playingInt);
                gameLevel_ID = 540 + playingLevel;
                break;

            case GAME_2_ID: //601-660
//                gameLevel_ID = 600 + _getGameLevel_ForGameLevel_ID(stageInt, playingInt);
                gameLevel_ID = 600 + playingLevel;
                break;
            case GAME_3_ID: //661-720
//                gameLevel_ID = 660 + _getGameLevel_ForGameLevel_ID(stageInt, playingInt);
                gameLevel_ID = 660 + playingLevel;
                break;
            case GAME_4_ID: //721-780
//                gameLevel_ID = 720 + _getGameLevel_ForGameLevel_ID(stageInt, playingInt);
                gameLevel_ID = 720 + playingLevel;
                break;

            default:
                Log.d(TAG, "unhandled playing level");
                return "-1";

        }

        Log.d(TAG, "gameLevel_ID: " + String.valueOf(gameLevel_ID));

        return String.valueOf(gameLevel_ID);
    }

    //need not use this method
    private static int _getGameLevel_ForGameLevel_ID(int stage, int playingLevel) {
        if ((stage < 4 && stage > 0) && (playingLevel < 21 && playingLevel > 0)) {
            switch (stage) {
                case 1:
                    return playingLevel;
                case 2:
                    return playingLevel + 0;
                case 3:
                    return playingLevel + 0;
            }
        } else {
            Log.d(TAG, "_getGameLevel_ForGameLevel_ID: " + "unhandled stage or playing level");
            return -1;

        }

        return -1;
    }


}
