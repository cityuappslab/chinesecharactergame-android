package appslab.chinesecharacteracquisitiongame.gamesetting;

import java.util.ArrayList;

/**
 * Created by developer on 7/19/16.
 * This class is for the GameLevelMenu to download the level data from the server
 * and then it will pass the relevant date to the Game Activity
 * In the Game Activity such as Game1, Game4, Game5_bird, Game_bat
 * they will use the level data passed from GameLevelMenu
 * and settle the duration of the game and the matrix of the words.
 */
public class GameLevelShared {
    public static GameLevelShared sharedInstance = null;
    public static ArrayList<GameLevel> GLArrayList = new ArrayList<>();

    public GameLevelShared() {
    }

    public ArrayList<GameLevel> getGameLevels () {
        return GLArrayList;
    }

    public void addLevel(GameLevel gameLevel) {
        GLArrayList.add(gameLevel);
    }

    public static GameLevelShared getInstance() {
        if(sharedInstance == null) {
            sharedInstance = new GameLevelShared();
        }
        return sharedInstance;
    }
}
