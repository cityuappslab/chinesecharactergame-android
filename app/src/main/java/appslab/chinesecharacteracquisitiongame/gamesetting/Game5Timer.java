package appslab.chinesecharacteracquisitiongame.gamesetting;
import android.app.Activity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ted on 25/5/2016.
 */
public class Game5Timer
{
    Activity activity;
    Double duration;
    Double decreaseTime=100.0;
    Double TotalTime;
    GameCoreData gcd;
    Timer timer;
    Double responseTimeCounter = 0.0;
    /*The purpose of this variable is used to recognize if the sound button is clicked.
      Why it exists?:
      Without this variable, the progress bar will still animate if the player pause the game and click resume button(Although the player haven't clicked the sound button to start yet),
      since it invokes the resume method of this timer directly in gameActivity.
      With the variable, we can know if the sound button is clicked and in order to decide if there is the need to animate the progress bar.
    */
    Boolean isSoundButtonClicked = false;

    public Game5Timer(GameCoreData gcd){
        timer = new Timer(true);
        duration = gcd.getDurationFromTrial();
        TotalTime = duration;
        this.gcd=gcd;
        //Reset the responseTimeCounter to 0.0 second
        responseTimeCounter = 0.0;
    }

    public void start(){
        timer = new Timer(true);
        timer.schedule(new Task(), 0, 100);
        gcd.setGameStatus("start");
        ((Game5Controller)activity).startAnimateProgressBar(TotalTime.intValue());
    }
    public void resume(){ timer = new Timer(true);timer.schedule(new Task(), 0, 100);((Game5Controller)activity).startAnimateProgressBar(TotalTime.intValue());gcd.setGameStatus("start");}
    public void stop(){timer.cancel();}
    public void updateTrial() {
        stop();
        setIsSoundButtonClicked(false);
        duration = gcd.getDurationFromTrial();
        TotalTime = duration;
        responseTimeCounter = 0.0;
        ((Game5Controller)activity).setProgressBar(100);
        ((Game5Controller)activity).createCharacter();
        ((Game5Controller)activity).setAnswer();
    }

    public double getResponseTime() {
        return responseTimeCounter;
    }

    public double getTime() {
        return TotalTime;
    }

    public void setIsSoundButtonClicked(boolean flag) {
        isSoundButtonClicked = flag;
    }

    public boolean getIsSoundButtonClicked() {
        return isSoundButtonClicked;
    }

    public void setActivity(Activity a){this.activity=a;}
    public class Task extends TimerTask
    {
        public void run(){
            TotalTime -= decreaseTime;
            responseTimeCounter += decreaseTime;

            if (TotalTime < 0) {
                ((Game5Controller)activity).processGameEnd();
                this.cancel();
            }
        }
    }
}