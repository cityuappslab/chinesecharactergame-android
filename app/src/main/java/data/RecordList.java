package data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by appslab on 6/28/16.
 */
public class RecordList {
    private static JSONArray recordList;
    private static final RecordList instance = new RecordList();


    private RecordList() {

        recordList = new JSONArray();


    }
    public static RecordList getInstance(){
        return instance;
    }
    public void addRecord(int trial,double responseTime,int numOfRow,int numOfCol,
                          String char1,String char2,int outcome) throws JSONException {
        JSONObject record = new JSONObject();
        record.put("trial",trial);
        record.put("responseTime",responseTime);
        record.put("numOfRow",numOfRow);
        record.put("numOfCol",numOfCol);
        record.put("char1",char1);
        record.put("char2",char2);
        record.put("outcome",outcome);
        recordList.put(record);
    }
    public static JSONArray getRecordList()
    {

        return recordList;
    }

}
